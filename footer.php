		
		<footer id="footer" class="footer">
			<div class="container">
				<div class="row">
				
					<div class="col-md-6 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-duration="500ms">
						<div class="footer-single">
							<h2>About Us</h2>
							<p>MIZAN IT LIMITED is Focused in Software and Web Design, Development, Search Engine Optimization .....</p>
							<a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
							  Read More
							</a>
							<div class="collapse" id="collapseExample">
							  <div class="well">
								<p>MIZAN IT LIMITED is focused in Software and Web Design, development, search engine optimization, E-Commerce & Graphic and Logo design. We have a highly capable team of web consultants, creative designers, content writers, programmers and web marketing professionals who know how to deliver results. We offer affordable low cost web design with our web design services for all personal web sites and business web sites. Our professionals believe that the key factor for the success of any ongoing project is to build a spotless communication bond with our client.</p>
							  </div>
							</div>
						
						</div>
					</div>
				
					<!-- <div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="300ms">
						<div class="footer-single">
							<h6>Subscribe </h6>
							<form action="#" class="subscribe">
								<input type="text" name="subscribe" id="subscribe">
								<input type="submit" value="&#8594;" id="subs">
							</form>
							<p>eusmod tempor incididunt ut labore et dolore magna aliqua. </p>
						</div>
					</div>-->
				
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="600ms">
						<div class="footer-single">
							<h6>Footer Menu</h6>
							<ul>
								<li class="current"><a href="#body">Home</a></li>
								<li><a href="#features">About Us</a></li>
								<li><a href="#service">Services</a></li>
							<!--<li><a href="#works">Skill</a></li>-->
								<li><a href="#Clients">Clients</a></li>
								<li><a href="#Payments">Payments</a></li>
							   <!-- <li><a href="#gallery">Gallery</a></li>-->
								<li><a href="#contact">Contact</a></li>
							</ul>
						</div>
					</div>
				
				<!--
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="900ms">
						<div class="footer-single">
							<!--===================Start Contatt from ================-->
                             
                        <div class="contact">
                            <?php
                            
                            $ok="";
                            $not_ok="";
                            if (isset($_POST['submit'])) {
                            	
                            	
                            	$name = $_POST['name'];
                                $email = $_POST['email'];
                                $message = $_POST['message'];
                                $from = 'From: http://mizan-it.com/'; 
                                $to = 'mizanitltd@gmail.com'; 
                                $subject = 'Email Inquiry';
                            
                                $body = "From: $name\n E-Mail: $email\n Message:\n $message";
                            	
                                if (mail ($to, $subject, $body, $from)) { 
                                    $ok= '<p>Thank you for your email!</p>';
                                } else { 
                                    $not_ok= '<p>Oops! An error occurred. Try sending your message again.</p>'; 
                                }
                            }
                            ?>
 
           <h3>Quick Contact From</h3>              
  <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
   &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; Click Here &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Quick Contact From</h4>
      </div>
      <div class="modal-body">
        
        <!--from stard-->
              <div class="form-group">
                             <form action="contact.php" method="post" enctype="multipart/form-data">

                                        <h3> <?php echo $ok ;?> </h3>
                                        <h3> <?php echo $not_ok ;?> </h3>
                                        
                        	        <h3 class="title">Contact From</h3>
                        	        
                        		<div class="form-group">
                        		    <input style="width:100%" name="name" required="required" placeholder="Your Name"/>
                        		</div>    
                        		            
                        		<div class="form-group">     
                        		    <input style="width:100%" name="email" type="email" required="required" placeholder="Your Email"/>
                        		</div>           
                        		    
                        		<div class="form-group">     
                        		    <textarea name="message" cols="66" rows="5" required="required" placeholder="Message"></textarea>
								</div>
								
								<div class="form-group"> 
                        		    <input id="submit" name="submit" type="submit" value="Submit">
                        	                                		    		    
                        		    <input type="reset" id="cancel" name="reset" value="Reset" />
								</div>
								
                        	    </form>
                            </div>
        <!-- from End-->
        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
                        
   </div>
    
     <!--===================End Contatt from ================-->  
						</div>
					</div>
					
				</div>
				<div class="row">
					<div class="col-md-12">
						<p class="copyright text-center">
							Copyright &copy; 2015 <a href="http://themefisher.com/"> Mizan IT Ltd.</a>. All rights reserved. Designed & developed by <a href="http://mizan-it.com/">Mizan IT Ltd.</a>
						</p>
					</div>
				</div>
			</div>
				<div class="icon">
					<a href="https://www.facebook.com/mdraju.molla"><i class="largeicon fa fa-facebook"></i></a>
					<a href="https://www.youtube.com/channel/UCrSQhmPR7frEaM-TqhyQXbw"><i class="largeicon fa fa-youtube-square"></i></a>
					<a href="https://twitter.com/rajucmt6"><i class="largeicon fa fa-twitter"></i></a>
				</div>
		</footer>
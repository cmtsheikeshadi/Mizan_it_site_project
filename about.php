<section id="features" class="features">
			<div class="container">
				<div class="row">
				
					<div class="sec-title text-center mb50 wow bounceInDown animated" data-wow-duration="500ms">
						<h2>About Us</h2>
						<div class="devider"><i class="fa fa-heart-o fa-lg"></i></div>
					</div>

					<!-- service item -->
					<div class="col-md-12 wow fadeInLeft" data-wow-duration="500ms">
						<div class="service-item text-justify">
							 
							
							<div class="service-desc">
								<h3 class="text-center">ABOUT MIZAN IT LIMITED</h3>
								<p>
                                
                                    MIZAN IT LIMITED is focused in Software and Web Design, 
                                    development, search engine optimization, E-Commerce & 
                                    Graphic and Logo design. We have a highly capable team of web consultants,
                                    creative designers, content writers,
                                    programmers and web marketing professionals who know how to deliver results.
                                    We offer affordable low cost web design with our web design
                                    services for all personal web sites and business web sites.
                                    Our professionals believe that the key factor for the success
                                    of any ongoing project is to build a spotless communication
                                    bond with our client. Our team members are not just IT professionals
                                    but have decent communication skills with people, which effort
                                    in cycle with your needs and requirements. Our company takes 
                                    care of every minute with details for what our client communicate
                                    during the process of developing the project. As a web development
                                    firm our request for you to look at the information technology 
                                    sector in Bangladesh in recent days. This industry has been improved 
                                    very rapidly with available highly skilled IT professionals and update 
                                    communication technologies. As you know because of a developing country 
                                    and currency advantage, it is a crystal-clear cost advantage for you to 
                                    outsource your web development work to us.

                                </p>
							</div>
						</div>
					</div>
					<!-- end service item -->
			  
						
				</div>
			</div>
		</section>
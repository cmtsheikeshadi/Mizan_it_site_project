		<section id="contact" class="contact">
			<div class="container">
				<div class="row mb50">
				
					<div class="sec-title text-center mb50 wow fadeInDown animated" data-wow-duration="500ms">
						<h2>CONTACT US </h2>
						<div class="devider"><i class="fa fa-heart-o fa-lg"></i></div>
					</div>
					
					<div class="sec-sub-title text-center wow rubberBand animated" data-wow-duration="1000ms">
						<p class="text-center">Please Contact Us. Filling The Following Contact Form.</p>
					</div>
					
					<!-- contact address -->
					<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 wow fadeInLeft animated" data-wow-duration="500ms">
						<div class="contact-address">
							<h3>Our Location!</h3>
							<p>558, Kazi Para (1st Floor)</p>
							<p>Mirpur, Dhaka -1216.</p>
							<p>Phone: +88 02 9030095</p>
						</div>
					</div>
					<!-- end contact address -->
					
					<!-- contact form -->
					<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12 wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="300ms">
					
<?php
    $name = $_POST['name'];
    $email = $_POST['email'];
    $message = $_POST['message'];
    $from = 'From: YourWebsite.com'; 
    $to = 'cmtsheikeshadi@gmail.com'; 
    $subject = 'Email Inquiry';

    $body = "From: $name\n E-Mail: $email\n Message:\n $message";
?>

<?php
if ($_POST['submit']) {
    if (mail ($to, $subject, $body, $from)) { 
        echo '<p>Thank you for your email!</p>';
    } else { 
        echo '<p>Oops! An error occurred. Try sending your message again.</p>'; 
    }
}
?>


<?php
	
	$ok="";
	$not_ok="";
	if (isset($_POST['submit'])) {
		
		
		$name = $_POST['name'];
		$email = $_POST['email'];
		$message = $_POST['message'];
		$from = 'From: http://mizan-it.com/'; 
		$to = 'mizanitltd@gmail.com'; 
		$subject = 'Email Inquiry';
	
		$body = "From: $name\n E-Mail: $email\n Message:\n $message";
		
		if (mail ($to, $subject, $body, $from)) { 
			$ok= '<p>Thank you for your email!</p>';
		} else { 
			$not_ok= '<p>Oops! An error occurred. Try sending your message again.</p>'; 
		}
	}
	?>
					
					
					
						<div class="contact-form">
							<h3>SAY HELLO!</h3>
							<form action="#" id="contact-form" method="post">
								<div class="input-group name-email">
									<div class="input-field">
										<input type="text" name="name" id="name" placeholder="Name" class="form-control">
									</div>
									<div class="input-field">
										<input type="email" name="email" id="email" placeholder="Email" class="form-control">
									</div>
								</div>
								<div class="input-group">
									<textarea name="message" id="message" placeholder="Message" class="form-control"></textarea>
								</div>
								<div class="input-group">
									<input type="submit" id="form-submit" name="submit" class="pull-right" value="Send message">
								</div>
							</form>
						</div>
					</div>
					<!-- end contact form -->
					
					<!-- footer social links -->
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 wow fadeInRight animated" data-wow-duration="500ms" data-wow-delay="600ms">
			<ul class="footer-social">
                            <li><a href="https://www.facebook.com/Themefisher"><i class="fa fa-facebook fa-2x"></i></a></li>
                            <li><a href="https://www.twitter.com/Themefisher"><i class="fa fa-twitter fa-2x"></i></a></li>
                            <li><a href="https://dribbble.com/themefisher"><i class="fa fa-dribbble fa-2x"></i></a></li>
							
			</ul>
					</div>
					<!-- end footer social links -->
					
				</div>
			</div>
			
			<!-- Google map -->
		<div id="map" class="wow bounceInDown animated" data-wow-duration="500ms"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d228.16284796715865!2d90.37295911655278!3d23.797056085548064!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0xd6c20c6c274b4df4!2sShwapno!5e0!3m2!1sen!2sbd!4v1449913431931" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe></div
			<!-- End Google map -->
			
		</section>
<a class="service1" href="#Domain_Registration" data-toggle="modal" data-target="#Domain_Registration"rel="works" title="Write Your Image Caption Here"><h3>View Details</h3><img src="img/works/item-3.jpg" alt=""></a>
		 
							<!-- Modal -->
							<div class="modal fade" id="Domain_Registration" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							  <div class="modal-dialog" role="document">
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 style="color:#1A7FEC; padding-left:10px; font-size:26px" class="modal-title" id="myModalLabel">Domain Registration:</h4>
								  </div>
								 
									<div id="tooplate_content">
											<p>Get your own .com, .net, .org, .info, .biz, .us, .name, Domain you can register or renew a domain name for just Yearly Tk. 1200/- There are no hidden fees! A domain name is your unique name on the Internet. It allows your company, organization or family to establish an Internet presence, consisting of your personalized email addresses and your own web site address.</p>
									    <div align="justify" class="style12">
											<table width="100%" cellspacing="0" cellpadding="0" border="0">
											  <tbody>
												<tr>
												  <td valign="top"><div align="center">
													  <table class="table table-striped" width="96%" cellspacing="0" cellpadding="0">
														<tbody>
														  <tr>
															<td width="16%"><p align="center" class="style30">TLD</p></td>
															<td width="12%"><p align="center" class="style30">Min. Years</p></td>
															<td width="18%"><p align="center" class="style30">Register</p></td>
															<td width="18%"><p align="center" class="style30">Transfer</p></td>
															<td width="36%"><p align="center" class="style30">Renew</p></td>
														  </tr>
														  <tr>
															<td><p align="center" class="style30">.com</p></td>
															<td><p align="center" class="style30">1</p></td>
															<td><p align="center" class="style30">Tk.1500.00 / $20</p></td>
															<td><p align="center" class="style30">Tk. 5000.00 / $65</p></td>
															<td><p align="center" class="style30">Tk. 1500.00 / $20</p></td>
														  </tr>
														  <tr>
															<td><p align="center" class="style30">.net</p></td>
															<td><p align="center" class="style30">1</p></td>
															<td><p align="center" class="style30">Tk. 1500.00 / $20</p></td>
															<td><p align="center" class="style30">Tk. 5000.00 / $65</p></td>
															<td><p align="center" class="style30">Tk. 1500.00 / $20</p></td>
														  </tr>
														  <tr>
															<td><p align="center" class="style30">.org</p></td>
															<td><p align="center" class="style30">1</p></td>
															<td><p align="center" class="style30">Tk. 1500.00 / $20</p></td>
															<td><p align="center" class="style30">Tk. 5000.00 / $65</p></td>
															<td><p align="center" class="style30">Tk. 1500.00 / $20</p></td>
														  </tr>
														  <tr>
															<td><p align="center" class="style30">.info</p></td>
															<td><p align="center" class="style30">1</p></td>
															<td><p align="center" class="style30">Tk. 1500.00 / $20</p></td>
															<td><p align="center" class="style30">Tk. 5000.00 / $65</p></td>
															<td><p align="center" class="style30">Tk. 1500.00 / $20</p></td>
														  </tr>
														  <tr>
															<td><p align="center" class="style30">.biz</p></td>
															<td><p align="center" class="style30">1</p></td>
															<td><p align="center" class="style30">Tk. 1500.00 / $20</p></td>
															<td><p align="center" class="style30">Tk. 5000.00 / $65</p></td>
															<td><p align="center" class="style30">Tk. 1500.00 / $20</p></td>
														  </tr>
														  <tr>
															<td><p align="center" class="style30">.com.bd/.net.bd</p></td>
															<td><p align="center" class="style30">1</p></td>
															<td><p align="center" class="style30">Tk. 5000.00 / $65</p></td>
															<td><p align="center" class="style30">Tk. 5000.00 / $65</p></td>
															<td><p align="center" class="style30">Tk. 5000.00 / $20</p></td>
														  </tr>
														</tbody>
													  </table>
												  </div></td>
												</tr>
											  </tbody>
											</table>
										  </div>
										<p align="justify"><strong>Included (free) in each domain name registration:</strong></p>
										<div align="justify">
										  <ul type="disc">
											<li>Free URL forwarding to the current website</li>
											<li>Free e-mail forwarding to your current mailbox</li>
											<li>Free POP3-mailbox (for use with email clients like Outlook etc)</li>
											<li>Free DNS services (self-install of A/MX/CNAME records)</li>
											<li>Free link to the current hosting package</li>
										  </ul>
										</div>
										<p align="justify"><strong>Terms &amp; Condition:</strong></p>
										<div align="justify">
										  <ul type="disc">
											<li>Client must renew the domain &amp; hosting before 1 month of expired date.</li>
											<li>If Client Failed to renew before one month then additional 2000 taka will add with renew charge</li>
											<li>If client want to renew after expired date then additional 8000 taka will add with renew charge</li>
											<li>If client failed to renew within 1 month of expired date then   the domain will permanently block and MIZAN IT LIMITED will not take any   responsibility to renew the domain</li>
											<li>Domain Control Panel / Transfer Cost is: 5000 taka / $65</li>
										  </ul>
										</div>
										<h2 style="color:#1A7FEC; padding-left:10px; font-size:26px" align="justify">Terms Of Service for Domain:</h2>
										<h2 style="color:#8DD241; padding-left:10px; font-size:20px" align="justify">1.0 ACCEPTANCE OF TERMS</h2>
										<p align="justify">The MIZAN IT LIMITED Domains   service (the "Service"), owned and operated by MIZAN IT LIMITED, is   provided to you under the most recent version of the MIZAN IT LIMITED   Terms of Service ("TOS"), the MIZAN IT LIMITED Small Business Privacy   Policy, the MIZAN IT LIMITED Site Guidelines, and any other MIZAN IT   LIMITED applicable policies and guidelines ("Guidelines") (all together,   the "Terms") as well as this Service Agreement (together the   "Agreement"). For clarity, MIZAN IT LIMITED Domains is a "Service" as   defined in the MIZAN IT LIMITED Terms of Service. MIZAN IT LIMITED may   update and change the Agreement from time to time. You can always find   the most recent version of this Service Agreement and the most recent   version of the MIZAN IT LIMITED Terms of Service at the URL indicated   above. Certain of the services that you purchase or receive from MIZAN   IT LIMITED may be provided by one or more vendors, contractors, or   affiliates selected by MIZAN IT LIMITED in its sole discretion. Any   capitalized term used herein but not otherwise defined herein shall have   the meaning set forth in the Terms.<br>
										  BY COMPLETING THE DOMAIN NAME (I.E., WEB ADDRESS) REGISTRATION   PROCESS, YOU AGREE TO BE BOUND BY THE AGREEMENT SO PLEASE READ ALL THE   TERMS CAREFULLY. ANY PERSON OR ENTITY ACTING ON YOUR BEHALF SHALL ALSO   BE BOUND BY THESE TERMS AND YOU AGREE TO BE RESPONSIBLE FOR SUCH   PERSON'S OR ENTITY'S ACTIONS.</p>
										<h2 style="color:#8DD241; padding-left:10px; font-size:20px"align="justify">2.0 THE SERVICE</h2>
										<p align="justify">Subject to the terms of the   Agreement, MIZAN IT will provide the Service which includes assisting   you in acquiring a domain name (i.e., web address) as well as providing   you access to certain MIZAN IT Software ("Software") to facilitate your   use of the Service.<br>
										  PLEASE NOTE: NOTHING IN THE AGREEMENT OBLIGATES MIZAN IT TO LIST OR   LINK TO YOUR DOMAIN NAME OR PROVIDE WEB SITE HOSTING SERVICES IN   CONNECTION WITH YOUR DOMAIN NAME BEYOND THAT PROVIDED WITHIN THE   SERVICE.<br>
										  You may obtain full-featured website hosting services from MIZAN IT Web Hosting</p>
										<h2 style="color:#8DD241; padding-left:10px; font-size:20px"align="left">3.0 YOUR AGREEMENT WITH INTERNET NAMES WORLDWIDE</h2>
										<p align="justify">If You register a new domain name in conjunction with the Service, the following terms also apply:<br>
										  MIZAN IT LIMITED has chosen Internet Names Worldwide (a division of   Melbourne IT Limited), or "INWW," an ICANN accredited registrar for   .com, .net, .org, .biz, .info and .us domain names, to provide domain   name registration services. You hereby authorize MIZAN IT LIMITED to   acquire your selected domain name from INWW. In order to receive a   domain name, you must agree to INWW's terms and conditions.</p>
										<h2 style="color:#8DD241; padding-left:10px; font-size:20px"align="justify">4.0 FEES AND BILLING</h2>
										<p align="justify">To pay for the Service, you must   have a MIZAN IT LIMITED ID. You must also have a MIZAN IT LIMITED Wallet   which will save your credit card, billing and shipping information and   other information. If you do not yet have a MIZAN IT LIMITED ID or MIZAN   IT LIMITED Wallet, you will be prompted to complete the registration   process.<br>
										  Domain Fee: 1500 taka / $65</p>
										<h2 style="color:#8DD241; padding-left:10px; font-size:20px"align="justify">5.0 RENEWAL</h2>
										<p align="justify">You have to pay the renew bill at   least 1 month before of expired date. If you failed to pay before 1   month then you have to pay additional 1500 taka for domain. If you want   to renew after expired the domain then you have to pay additional 8000   taka. If you failed to pay the renew bill with in 1 month after expired   then the domain will block permanently.</p>
										<h2 style="color:#8DD241; padding-left:10px; font-size:20px"align="left">6.0 TRANSFERRING TO MIZAN IT'S REGISTRAR OF RECORD</h2>
										<p align="justify">If you want to transfer your domain   to other company then you have to pay 5,000 taka or $65 for domain   transfer fee. After your transfer we do not have any liabilities about   your domain.</p>
										<h2 style="color:#8DD241; padding-left:10px; font-size:20px"align="justify">7.0 USING A PREEXISTING DOMAIN NAME</h2>
										<p align="justify">If you have previously registered a   domain name with another provider and want to use it with the Service,   You must request that the existing registrar change the name servers for   the domain name as designated by MIZAN IT, on Your behalf.<br>
										  PLEASE NOTE: THE EXISTING REGISTRAR WILL CONTINUE TO BE THE   REGISTRAR FOR THAT DOMAIN NAME AND YOU WILL CONTINUE TO BE RESPONSIBLE   FOR ALL ONGOING FEES FOR THAT DOMAIN NAME WITH YOUR EXISTING PROVIDER,   INCLUDING RENEWAL FEES. THE FEES PAYABLE TO MIZAN IT FOR THE SERVICE DO   NOT INCLUDE REGISTRATION OR RENEWAL FEES OWED BY YOU TO YOUR EXISTING   PROVIDER.</p>
										<h2 style="color:#8DD241; padding-left:10px; font-size:20px"align="justify">8.0 VERIFYING YOUR DOMAIN NAME INFORMATION</h2>
										<p align="justify">In compliance with ICANN regulation   and the terms and conditions of the Registrar of Record, as applicable   ("Required Information") and in order to minimize the risk of fraud,   MIZAN IT LIMITED may at any time request You to verify any information   required to be supplied by a registrant. If You fail to respond to any   such request or fail to verify any Required Information to MIZAN IT   LIMITED's reasonable satisfaction, within 15 days of any such request   from MIZAN IT LIMITED, MIZAN IT LIMITED may, in its sole discretion,   immediately terminate Your Service and remove any of Your materials,   including Your domain name, from MIZAN IT LIMITED servers.</p>
										<h2 style="color:#8DD241; padding-left:10px; font-size:20px"align="justify">9.0 INDEMNITY</h2>
										<p align="justify">You agree to indemnify and hold   MIZAN IT LIMITED, and its subsidiaries, affiliates, officers, agents,   co-branders or other partners, and employees, harmless from any claim or   demand, including reasonable attorneys' fees, made by any third party   due to or arising out of Content you submit, post, transmit or make   available through the Service, your use of the Service, your connection   to the Service, your violation of the Agreement or your violation of any   laws, regulations or rights of another.</p>
										<h2 style="color:#8DD241; padding-left:10px; font-size:20px"align="justify">10.0 MAINTENANCE AND SUPPORT</h2>
										<p align="justify">You can obtain assistance with   technical difficulties that may arise in connection with your   utilization of the Software or the Service by contacting customer   care.MIZAN IT LIMITED reserves the right to establish limitations on the   extent of such support, and the hours at which it is available.<br>
										  You are responsible for obtaining and maintaining all telephone,   computer hardware and other equipment needed for its access to and use   of the Software and the Service and you will be responsible for all   charges related thereto</p>
										<h4>&nbsp;</h4>
									</div>
								  
								  
								  <div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									
								  </div>
								</div>
							  </div>
							</div>
						<h4 class="text-center" style="color:#F02D28;text-width:bold;">Domain Registration </h4>
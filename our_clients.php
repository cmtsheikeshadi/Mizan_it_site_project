		<section id="Clients" class="Clients">
			<div class="container">
				<div class="row">
		
					<div class="sec-title text-center wow fadeInUp animated" data-wow-duration="700ms">
						<h2>List Of Some Clients </h2>
						<div class="devider"><i class="fa fa-heart-o fa-lg"></i></div>
					</div>
					
					 <!-- single member -->
					<figure class="team-member col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms">
					  
                    	             <div class="member-thumb">
                    	                     	<a href="http://electronicshaat.com/" target="_blank">
							<img src="img/team/member-7.jpg" alt="Team Member" class="img-responsive">
							<figcaption class="overlay">
				<h5> Company Name: <br /> <strong style="color: #fff;"> Electronics Haat </strong></br><b> Visite This Site </b></h5>
								
							 </figcaption>
							 </a>
						</div>
						
					 
					</figure>
					<!-- end single member -->
					
				 
					
					<!-- single member -->
					<figure class="team-member col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms">
					  
                    	<div class="member-thumb">
                    	<a href="http://shari-bazar.com/" target="_blank" >
							<img src="img/team/member-8.jpg" alt="Team Member" class="img-responsive">
							<figcaption class="overlay">
						<h5> Company Name:<br /> <strong style="color: #fff;"> Shari Bazar.com</strong></br><b>Visite This Site</b> </h5>
								
							 </figcaption>
							 </a>
						</div>
						
					 
					</figure>
					<!-- end single member -->
					
					
					<!-- single member -->
					<figure class="team-member col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms">
					  
                    	<div class="member-thumb">
                    	<a href="http://megamall24.com/" target="_blank" >
							<img src="img/team/member-1.png" alt="Team Member" class="img-responsive">
							<figcaption class="overlay">
					<h5> Company Name: <br /> <strong style="color: #fff;"> MEGAMALL24.COM </strong></br><b> Visite This Site </b> </h5>
								
							 </figcaption>
							  </a>
						</div>
						
					 
					</figure>
					<!-- end single member -->
                    
                    	<!-- single member -->
					<figure class="team-member col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms">
					  
                    	<div class="member-thumb">
                    	<a href="http://soptorshi-technology.com/" target="_blank" >
							<img src="img/team/member-4.jpg" alt="Team Member" class="img-responsive">
							<figcaption class="overlay">
		<h5> Company Name: <br /> <strong style="color: #fff;"> Soptorshi Technology Limited. </strong></br><b> Visite This Site </b> </h5>
								
							 </figcaption>
							 </a>
						</div>
						
					 
					</figure>
					<!-- end single member -->
                    
                 
                    
                     <!-- single member -->
					<figure class="team-member col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms">
					  
                    	<div class="member-thumb">
                    	<a href="http://bfastit.com/" target="_blank">
							<img src="img/team/member-6.png" alt="Team Member" class="img-responsive">
							<figcaption class="overlay">
				<h5> Company Name: <br /> <strong style="color: #fff;"> B-first IT Limited </strong></br><b> Visite This Site </b> </h5>
								
							 </figcaption>
							 </a>
						</div>
						
					 
					</figure>
					<!-- end single member -->
                    
                    
                       <!-- single member -->
					<figure class="team-member col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms">
					  
                    	<div class="member-thumb">
                    	<a href="http://www.lasvalley.net/" target="_blank">
							<img src="img/team/member-5.png" alt="Team Member" class="img-responsive">
							<figcaption class="overlay">
					<h5> Company Name: <br /> <strong style="color: #fff;"> Lasvalley International Corporation </strong> </br><b> Visite This Site </b></h5>
								
							 </figcaption>
							</a> 
						</div>
						
					 
					</figure>
					<!-- end single member -->
                    
                    
                     <!-- single member -->
					<figure class="team-member col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms">
					  
                    	<div class="member-thumb">
                    	<a href="http://www.modernitltd.com/" target="_blank">
							<img src="img/team/member-2.jpg" alt="Team Member" class="img-responsive">
							<figcaption class="overlay">
								<h5> Company Name: <br /> <strong style="color: #fff;"> Modern IT Ltd. </strong></br><b> Visite This Site </b> </h5>
							 </figcaption>
							 </a>
						</div>
						
					 
					</figure>
					<!-- end single member -->
					
					
					<!-- single member -->
					<figure class="team-member col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms">
					  
                    	<div class="member-thumb">
                    	<a href="http://www.kmcitlimited.com/" target="_blank">
							<img src="img/team/member-2.png" alt="Team Member" class="img-responsive">
							<figcaption class="overlay">
								<h5> Company Name: <br /> <strong style="color: #fff;"> KMC IT LIMITED</strong></br><b> Visite This Site </b> </h5>
							 </figcaption>
							 </a>
						</div>
						
					 
					</figure>
					<!-- end single member -->
                    
                    
					
				</div>
			</div>
		</section>
		
<!DOCTYPE html>

    <head>
    	<!-- meta charec set -->
        <meta charset="utf-8">
		<!-- Always force latest IE rendering engine or request Chrome Frame -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<!-- Page Title -->
        <title>Mizan IT Limited || Excelence of IT Professional</title>		
		<!-- Meta Description -->
        
        <meta name="description" content="web development,e-commerce site development"/>
        
        <meta name="keywords" content="Web Design and Development,E-Commerce,Domain Registration,Hosting Server,Search Engine Optimization,E-mail Marketinge,SMS Marketing,Software Development,Logo Design">
        <meta name="author" content="Muhammad Morshed">
		<!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!-- Google Font -->
		<link rel="shortcut icon" href="img/logo.png" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>

		<!-- CSS
		================================================== -->
		<!-- Fontawesome Icon font -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
		<!-- Twitter Bootstrap css -->
                <link href="css/pgwslider.min.css" rel="stylesheet" type="text/css"/>
		<link href="css/owl.carousel.css" rel="stylesheet" type="text/css"/>
		<link href="css/owl.transitions.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="css/bootstrap.min.css">
		<!-- jquery.fancybox  -->
        <link rel="stylesheet" href="css/jquery.fancybox.css">
		<!-- animate -->
        <link rel="stylesheet" href="css/animate.css">
		<!-- Main Stylesheet -->
        <link rel="stylesheet" href="css/main.css">
		<!-- media-queries -->
        <link rel="stylesheet" href="css/media-queries.css">
        <link rel="stylesheet" href="css/tooplate_style.css">

		
		<!-- Modernizer Script for old Browsers -->
        <script src="js/modernizr-2.6.2.min.js"></script>
		
    </head>
	
    <body id="body">
	
		<!-- preloader -->
		<!--<div id="preloader">
			<img src="img/preloader.gif" alt="Preloader">
		</div>-->
		<!-- end preloader -->

        <!-- 
        Fixed Navigation
        ==================================== -->
        <header id="navigation" class="navbar-fixed-top navbar">
            <div class="container">
                <div class="navbar-header">
                    <!-- responsive nav button -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-bars fa-2x"></i>
                    </button>
					<!-- /responsive nav button -->
					
					<!-- logo -->
                    <a class="navbar-brand" href="#body">
						<h1 id="logo">
							<img src="img/logo.png" alt="Brandi">
						</h1>
					</a>
					<!-- /logo -->
                </div>
					<h1><a class="top" href="#">Mizan IT Limited</a></h1>
				<!-- main nav -->
                <nav class="collapse navbar-collapse navbar-right" role="navigation">
                    <ul id="nav" class="nav navbar-nav">
                        <li class="current"><a href="#body">Home</a></li>
                        <li><a href="#features">About Us</a></li>
                        <li><a href="#service">Services</a></li>
                    <!--<li><a href="#works">Skill</a></li>-->
                        <li><a href="#Clients">Clients</a></li>
                        <li><a href="#Payments">Payments</a></li>
                       <!-- <li><a href="#gallery">Gallery</a></li>-->
                        <li><a href="#contact">Contact</a></li>
                    </ul>
                </nav>
				<!-- /main nav -->
				
            </div>
        </header>
<?php require_once('header.php');?>
        <!--
        End Fixed Navigation
        ==================================== -->
		
		
		
        <!--
        Home Slider
        ==================================== -->
		
		<?php require_once('slider.php');?>
		
        <!--
        End Home SliderEnd
        ==================================== -->
		
        <!--
        About
        ==================================== -->
		
		<?php require_once('about.php');?>
		
        <!--
        End Features
        ==================================== -->
		
		
        <!--
        Our Servise
        ==================================== -->
		
		<?php require_once('service.php');?>
		
        <!--
        End Our Servise
        ==================================== -->
        
<section id="carosel_area">
		<h3 class="text-center" style="padding-bottom:20px;">Technologies We Use</h3>
			<div class="container">
				<div id="owl-demo" class="owl-carousel owl-theme">
					  <div class="item"><h1><img src="img/carosel/1.jpg"/></h1></div>
					  <div class="item"><h1><img src="img/carosel/2.jpg"/></h1></div>
					  <div class="item"><h1><img src="img/carosel/6.jpg"/></h1></div>
					  <div class="item"><h1><img src="img/carosel/4.jpg"/></h1></div>
					  <div class="item"><h1><img src="img/carosel/7.jpg"/></h1></div>
					  <div class="item"><h1><img src="img/carosel/6.jpg"/></h1></div>
					  <div class="item"><h1><img src="img/carosel/2.jpg"/></h1></div>
					  <div class="item"><h1><img src="img/carosel/9.jpg"/></h1></div>
					  <div class="item"><h1><img src="img/carosel/1.jpg"/></h1></div>
					  <div class="item"><h1><img src="img/carosel/4.jpg"/></h1></div>
					  <div class="item"><h1><img src="img/carosel/3.jpg"/></h1></div>
					  <div class="item"><h1><img src="img/carosel/6.jpg"/></h1></div>
					  <div class="item"><h1><img src="img/carosel/10.jpg"/></h1></div>
						<div class="item"><h1><img src="img/carosel/4.jpg"/></h1></div>
					   <div class="item"><h1><img src="img/carosel/6.jpg"/></h1></div>
				</div>
				
			</div>
		</section>
        
         <!--
        Our Clients
        ==================================== -->
		<?php require_once('our_clients.php'); ?>
        <!--
        End Our Clients
        ==================================== -->
        
		<!--
        Payments
        ==================================== -->		
		<?php require_once('payments.php');?>
        <!--
        End Payments
        ==================================== -->
		
		
		<!--
        Contact Us
        ==================================== -->		
		<?php require_once('contact_us.php')?>
        <!--
        End Contact Us
        ==================================== -->
		
		<?php require_once('footer.php');?>
		
		<a href="javascript:void(0);" id="back-top"><i class="fa fa-angle-up fa-3x"></i></a>

		<!-- Essential jQuery Plugins
		================================================== -->
		<!-- Main jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<!-- Single Page Nav -->
        <script src="js/jquery.singlePageNav.min.js"></script>
		<!-- Twitter Bootstrap -->
        <script src="js/bootstrap.min.js"></script>
		
		<!-- jquery.fancybox.pack -->
        <script src="js/jquery.fancybox.pack.js"></script>
		<!-- jquery.mixitup.min -->
        <script src="js/jquery.mixitup.min.js"></script>
		<!-- jquery.parallax -->
        <script src="js/jquery.parallax-1.1.3.js"></script>
		<!-- jquery.countTo -->
        <script src="js/jquery-countTo.js"></script>
		<!-- jquery.appear -->
        <script src="js/jquery.appear.js"></script>
		<!-- Contact form validation -->
		<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.32/jquery.form.js"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.11.1/jquery.validate.min.js"></script>
		<!-- Google Map -->
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
		<!-- jquery easing -->
        <script src="js/jquery.easing.min.js"></script>
		<!-- jquery easing -->
        <script src="js/wow.min.js"></script>
		

                 <script type="text/javascript" src="js/pgwslider.min.js" ></script>
		 <script type="text/javascript" src="js/owl.carousel.min.js" ></script>
		
		<script>
			var wow = new WOW ({
				boxClass:     'wow',      // animated element css class (default is wow)
				animateClass: 'animated', // animation css class (default is animated)
				offset:       120,          // distance to the element when triggering the animation (default is 0)
				mobile:       false,       // trigger animations on mobile devices (default is true)
				live:         true        // act on asynchronously loaded content (default is true)
			  }
			);
			wow.init();
		</script> 
		<!-- Custom Functions -->
        <script src="js/custom.js"></script>
		
		<script type="text/javascript">
			$(function(){
				/* ========================================================================= */
				/*	Contact Form
				/* ========================================================================= */
				
				$('#contact-form').validate({
					rules: {
						name: {
							required: true,
							minlength: 2
						},
						email: {
							required: true,
							email: true
						},
						message: {
							required: true
						}
					},
					messages: {
						name: {
							required: "come on, you have a name don't you?",
							minlength: "your name must consist of at least 2 characters"
						},
						email: {
							required: "no email, no message"
						},
						message: {
							required: "um...yea, you have to write something to send this form.",
							minlength: "thats all? really?"
						}
					},
					submitHandler: function(form) {
						$(form).ajaxSubmit({
							type:"POST",
							data: $(form).serialize(),
							url:"process.php",
							success: function() {
								$('#contact-form :input').attr('disabled', 'disabled');
								$('#contact-form').fadeTo( "slow", 0.15, function() {
									$(this).find(':input').attr('disabled', 'disabled');
									$(this).find('label').css('cursor','default');
									$('#success').fadeIn();
								});
							},
							error: function() {
								$('#contact-form').fadeTo( "slow", 0.15, function() {
									$('#error').fadeIn();
								});
							}
						});
					}
				});
			});
		</script>
<!-- carosel jquery start-->
		<script>
			
			$(document).ready(function() {
			$('.pgwSlider').pgwSlider();
				});
				
				$(document).ready(function() {
		 
		  var owl = $("#owl-demo");
		 
		  owl.owlCarousel({
			  items : 10, //10 items above 1000px browser width
			  itemsDesktop : [1000,5], //5 items between 1000px and 901px
			  itemsDesktopSmall : [900,3], // betweem 900px and 601px
			  itemsTablet: [600,2], //2 items between 600 and 0
			  itemsMobile : false ,// itemsMobile disabled - inherit from itemsTablet option
			  autoPlay: 1500,
		  });
		 
		  // Custom Navigation Events
		  $(".next").click(function(){
			owl.trigger('owl.next');
		  })
		  $(".prev").click(function(){
			owl.trigger('owl.prev');
		  })
		  $(".play").click(function(){
			owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
		  })
		  $(".stop").click(function(){
			owl.trigger('owl.stop');
		  })
		 
		});
		//scroltop
					$(window).scroll(function () {
						if ($(this).scrollTop() > 300) {
							$('.scrollup').fadeIn();
						} else {
							$('.scrollup').fadeOut();
						}
					});

					$('.scrollup').click(function () {
						$("html, body").animate({
							scrollTop: 0
						}, 600);
						return false;
					});
		</script>
		
		
		
		<!-- carosel end-->

    </body>
</html>

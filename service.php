<section id="service" class="service clearfix">
			<div class="container">
				<div class="row">
				
					<div class="sec-title text-center">
						<h2>Services</h2>
						<div class="devider"><i class="fa fa-heart-o fa-lg"></i></div>
					</div>
				
					
					<div class="work-filter wow fadeInRight animated" data-wow-duration="500ms">
						<ul class="text-center">
							<li><a href="javascript:;" data-filter="all" class="active filter">All</a></li>
							<li><a href="javascript:;" data-filter=".Domain_Registration" class="filter">Domain Registration</a></li>
							<li><a href="javascript:;" data-filter=".Hosting_Server" class="filter">Hosting Server</a></li>
							<li><a href="javascript:;" data-filter=".Web_Design" class="filter">Web Design & Development</a></li>
							<li><a href="javascript:;" data-filter=".E_Commerce" class="filter">E-Commerce</a></li>
                            <li><a href="javascript:;" data-filter=".SEO" class="filter">Search Engine Optimization</a></li>
                            <li><a href="javascript:;" data-filter=".mail_Marketinge" class="filter">E-mail Marketinge</a></li>
                            <li><a href="javascript:;" data-filter=".SMS_Marketing" class="filter">SMS Marketing</a></li>
                            <li><a href="javascript:;" data-filter=".Software_Development" class="filter">Software Development</a></li>
                            <li><a href="javascript:;" data-filter=".Logo_Design" class="filter">Logo Design</a></li>
						</ul>
					</div>
					
				</div>
			</div>
			
			<div class="project-wrapper">
			
				<figure class="mix work-item Domain_Registration">
					<a class="service1" href="#Domain_Registration" data-toggle="modal" data-target="#Domain_Registration"rel="works" title="Write Your Image Caption Here"><h3>View Details</h3><img src="img/works/item-3.jpg" alt=""></a>
		 
							<!-- Modal -->
							<div class="modal fade" id="Domain_Registration" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							  <div class="modal-dialog" role="document">
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 style="color:#1A7FEC; padding-left:10px; font-size:26px" class="modal-title" id="myModalLabel">Domain Registration:</h4>
								  </div>
								 
									<div id="tooplate_content">
											<p>Get your own .com, .net, .org, .info, .biz, .us, .name, Domain you can register or renew a domain name for just Yearly Tk. 1200/- There are no hidden fees! A domain name is your unique name on the Internet. It allows your company, organization or family to establish an Internet presence, consisting of your personalized email addresses and your own web site address.</p>
									    <div align="justify" class="style12">
											<table width="100%" cellspacing="0" cellpadding="0" border="0">
											  <tbody>
												<tr>
												  <td valign="top"><div align="center">
													  <table class="table table-striped" width="96%" cellspacing="0" cellpadding="0">
														<tbody>
														  <tr>
															<td width="16%"><p align="center" class="style30">TLD</p></td>
															<td width="12%"><p align="center" class="style30">Min. Years</p></td>
															<td width="18%"><p align="center" class="style30">Register</p></td>
															<td width="18%"><p align="center" class="style30">Transfer</p></td>
															<td width="36%"><p align="center" class="style30">Renew</p></td>
														  </tr>
														  <tr>
															<td><p align="center" class="style30">.com</p></td>
															<td><p align="center" class="style30">1</p></td>
															<td><p align="center" class="style30">Tk.1500.00 / $20</p></td>
															<td><p align="center" class="style30">Tk. 5000.00 / $65</p></td>
															<td><p align="center" class="style30">Tk. 1500.00 / $20</p></td>
														  </tr>
														  <tr>
															<td><p align="center" class="style30">.net</p></td>
															<td><p align="center" class="style30">1</p></td>
															<td><p align="center" class="style30">Tk. 1500.00 / $20</p></td>
															<td><p align="center" class="style30">Tk. 5000.00 / $65</p></td>
															<td><p align="center" class="style30">Tk. 1500.00 / $20</p></td>
														  </tr>
														  <tr>
															<td><p align="center" class="style30">.org</p></td>
															<td><p align="center" class="style30">1</p></td>
															<td><p align="center" class="style30">Tk. 1500.00 / $20</p></td>
															<td><p align="center" class="style30">Tk. 5000.00 / $65</p></td>
															<td><p align="center" class="style30">Tk. 1500.00 / $20</p></td>
														  </tr>
														  <tr>
															<td><p align="center" class="style30">.info</p></td>
															<td><p align="center" class="style30">1</p></td>
															<td><p align="center" class="style30">Tk. 1500.00 / $20</p></td>
															<td><p align="center" class="style30">Tk. 5000.00 / $65</p></td>
															<td><p align="center" class="style30">Tk. 1500.00 / $20</p></td>
														  </tr>
														  <tr>
															<td><p align="center" class="style30">.biz</p></td>
															<td><p align="center" class="style30">1</p></td>
															<td><p align="center" class="style30">Tk. 1500.00 / $20</p></td>
															<td><p align="center" class="style30">Tk. 5000.00 / $65</p></td>
															<td><p align="center" class="style30">Tk. 1500.00 / $20</p></td>
														  </tr>
														  <tr>
															<td><p align="center" class="style30">.com.bd/.net.bd</p></td>
															<td><p align="center" class="style30">1</p></td>
															<td><p align="center" class="style30">Tk. 5000.00 / $65</p></td>
															<td><p align="center" class="style30">Tk. 5000.00 / $65</p></td>
															<td><p align="center" class="style30">Tk. 5000.00 / $20</p></td>
														  </tr>
														</tbody>
													  </table>
												  </div></td>
												</tr>
											  </tbody>
											</table>
										  </div>
										<p align="justify"><strong>Included (free) in each domain name registration:</strong></p>
										<div align="justify">
										  <ul type="disc">
											<li>Free URL forwarding to the current website</li>
											<li>Free e-mail forwarding to your current mailbox</li>
											<li>Free POP3-mailbox (for use with email clients like Outlook etc)</li>
											<li>Free DNS services (self-install of A/MX/CNAME records)</li>
											<li>Free link to the current hosting package</li>
										  </ul>
										</div>
										<p align="justify"><strong>Terms &amp; Condition:</strong></p>
										<div align="justify">
										  <ul type="disc">
											<li>Client must renew the domain &amp; hosting before 1 month of expired date.</li>
											<li>If Client Failed to renew before one month then additional 2000 taka will add with renew charge</li>
											<li>If client want to renew after expired date then additional 8000 taka will add with renew charge</li>
											<li>If client failed to renew within 1 month of expired date then   the domain will permanently block and MIZAN IT LIMITED will not take any   responsibility to renew the domain</li>
											<li>Domain Control Panel / Transfer Cost is: 5000 taka / $65</li>
										  </ul>
										</div>
										<h2 style="color:#1A7FEC; padding-left:10px; font-size:26px" align="justify">Terms Of Service for Domain:</h2>
										<h2 style="color:#8DD241; padding-left:10px; font-size:20px" align="justify">1.0 ACCEPTANCE OF TERMS</h2>
										<p align="justify">The MIZAN IT LIMITED Domains   service (the "Service"), owned and operated by MIZAN IT LIMITED, is   provided to you under the most recent version of the MIZAN IT LIMITED   Terms of Service ("TOS"), the MIZAN IT LIMITED Small Business Privacy   Policy, the MIZAN IT LIMITED Site Guidelines, and any other MIZAN IT   LIMITED applicable policies and guidelines ("Guidelines") (all together,   the "Terms") as well as this Service Agreement (together the   "Agreement"). For clarity, MIZAN IT LIMITED Domains is a "Service" as   defined in the MIZAN IT LIMITED Terms of Service. MIZAN IT LIMITED may   update and change the Agreement from time to time. You can always find   the most recent version of this Service Agreement and the most recent   version of the MIZAN IT LIMITED Terms of Service at the URL indicated   above. Certain of the services that you purchase or receive from MIZAN   IT LIMITED may be provided by one or more vendors, contractors, or   affiliates selected by MIZAN IT LIMITED in its sole discretion. Any   capitalized term used herein but not otherwise defined herein shall have   the meaning set forth in the Terms.<br>
										  BY COMPLETING THE DOMAIN NAME (I.E., WEB ADDRESS) REGISTRATION   PROCESS, YOU AGREE TO BE BOUND BY THE AGREEMENT SO PLEASE READ ALL THE   TERMS CAREFULLY. ANY PERSON OR ENTITY ACTING ON YOUR BEHALF SHALL ALSO   BE BOUND BY THESE TERMS AND YOU AGREE TO BE RESPONSIBLE FOR SUCH   PERSON'S OR ENTITY'S ACTIONS.</p>
										<h2 style="color:#8DD241; padding-left:10px; font-size:20px"align="justify">2.0 THE SERVICE</h2>
										<p align="justify">Subject to the terms of the   Agreement, MIZAN IT will provide the Service which includes assisting   you in acquiring a domain name (i.e., web address) as well as providing   you access to certain MIZAN IT Software ("Software") to facilitate your   use of the Service.<br>
										  PLEASE NOTE: NOTHING IN THE AGREEMENT OBLIGATES MIZAN IT TO LIST OR   LINK TO YOUR DOMAIN NAME OR PROVIDE WEB SITE HOSTING SERVICES IN   CONNECTION WITH YOUR DOMAIN NAME BEYOND THAT PROVIDED WITHIN THE   SERVICE.<br>
										  You may obtain full-featured website hosting services from MIZAN IT Web Hosting</p>
										<h2 style="color:#8DD241; padding-left:10px; font-size:20px"align="left">3.0 YOUR AGREEMENT WITH INTERNET NAMES WORLDWIDE</h2>
										<p align="justify">If You register a new domain name in conjunction with the Service, the following terms also apply:<br>
										  MIZAN IT LIMITED has chosen Internet Names Worldwide (a division of   Melbourne IT Limited), or "INWW," an ICANN accredited registrar for   .com, .net, .org, .biz, .info and .us domain names, to provide domain   name registration services. You hereby authorize MIZAN IT LIMITED to   acquire your selected domain name from INWW. In order to receive a   domain name, you must agree to INWW's terms and conditions.</p>
										<h2 style="color:#8DD241; padding-left:10px; font-size:20px"align="justify">4.0 FEES AND BILLING</h2>
										<p align="justify">To pay for the Service, you must   have a MIZAN IT LIMITED ID. You must also have a MIZAN IT LIMITED Wallet   which will save your credit card, billing and shipping information and   other information. If you do not yet have a MIZAN IT LIMITED ID or MIZAN   IT LIMITED Wallet, you will be prompted to complete the registration   process.<br>
										  Domain Fee: 1500 taka / $65</p>
										<h2 style="color:#8DD241; padding-left:10px; font-size:20px"align="justify">5.0 RENEWAL</h2>
										<p align="justify">You have to pay the renew bill at   least 1 month before of expired date. If you failed to pay before 1   month then you have to pay additional 1500 taka for domain. If you want   to renew after expired the domain then you have to pay additional 8000   taka. If you failed to pay the renew bill with in 1 month after expired   then the domain will block permanently.</p>
										<h2 style="color:#8DD241; padding-left:10px; font-size:20px"align="left">6.0 TRANSFERRING TO MIZAN IT'S REGISTRAR OF RECORD</h2>
										<p align="justify">If you want to transfer your domain   to other company then you have to pay 5,000 taka or $65 for domain   transfer fee. After your transfer we do not have any liabilities about   your domain.</p>
										<h2 style="color:#8DD241; padding-left:10px; font-size:20px"align="justify">7.0 USING A PREEXISTING DOMAIN NAME</h2>
										<p align="justify">If you have previously registered a   domain name with another provider and want to use it with the Service,   You must request that the existing registrar change the name servers for   the domain name as designated by MIZAN IT, on Your behalf.<br>
										  PLEASE NOTE: THE EXISTING REGISTRAR WILL CONTINUE TO BE THE   REGISTRAR FOR THAT DOMAIN NAME AND YOU WILL CONTINUE TO BE RESPONSIBLE   FOR ALL ONGOING FEES FOR THAT DOMAIN NAME WITH YOUR EXISTING PROVIDER,   INCLUDING RENEWAL FEES. THE FEES PAYABLE TO MIZAN IT FOR THE SERVICE DO   NOT INCLUDE REGISTRATION OR RENEWAL FEES OWED BY YOU TO YOUR EXISTING   PROVIDER.</p>
										<h2 style="color:#8DD241; padding-left:10px; font-size:20px"align="justify">8.0 VERIFYING YOUR DOMAIN NAME INFORMATION</h2>
										<p align="justify">In compliance with ICANN regulation   and the terms and conditions of the Registrar of Record, as applicable   ("Required Information") and in order to minimize the risk of fraud,   MIZAN IT LIMITED may at any time request You to verify any information   required to be supplied by a registrant. If You fail to respond to any   such request or fail to verify any Required Information to MIZAN IT   LIMITED's reasonable satisfaction, within 15 days of any such request   from MIZAN IT LIMITED, MIZAN IT LIMITED may, in its sole discretion,   immediately terminate Your Service and remove any of Your materials,   including Your domain name, from MIZAN IT LIMITED servers.</p>
										<h2 style="color:#8DD241; padding-left:10px; font-size:20px"align="justify">9.0 INDEMNITY</h2>
										<p align="justify">You agree to indemnify and hold   MIZAN IT LIMITED, and its subsidiaries, affiliates, officers, agents,   co-branders or other partners, and employees, harmless from any claim or   demand, including reasonable attorneys' fees, made by any third party   due to or arising out of Content you submit, post, transmit or make   available through the Service, your use of the Service, your connection   to the Service, your violation of the Agreement or your violation of any   laws, regulations or rights of another.</p>
										<h2 style="color:#8DD241; padding-left:10px; font-size:20px"align="justify">10.0 MAINTENANCE AND SUPPORT</h2>
										<p align="justify">You can obtain assistance with   technical difficulties that may arise in connection with your   utilization of the Software or the Service by contacting customer   care.MIZAN IT LIMITED reserves the right to establish limitations on the   extent of such support, and the hours at which it is available.<br>
										  You are responsible for obtaining and maintaining all telephone,   computer hardware and other equipment needed for its access to and use   of the Software and the Service and you will be responsible for all   charges related thereto</p>
										<h4>&nbsp;</h4>
									</div>
								  
								  
								  <div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									
								  </div>
								</div>
							  </div>
							</div>
						<h4 class="text-center" style="color:#F02D28;text-width:bold;">Domain Registration </h4>
				</figure>
				
				 <figure class="mix work-item Hosting_Server">
					<a class="service1" href="#Hosting_Server" data-toggle="modal" data-target="#Hosting_Server"rel="works" title="Write Your Image Caption Here"><h3>View Details</h3><img src="img/works/item-4.jpg" alt=""></a>
		 
							<!-- Modal -->
							<div class="modal fade" id="Hosting_Server" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							  <div class="modal-dialog" role="document">
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h2 style="color:#1A7FEC; font-width:bold;" class="modal-title" id="myModalLabel"> Linux  Hosting Server</h2>
								  </div>
									<div id="tooplate_content">
													<p>Trust your website with a company that now hosts over 12,000 domains     on its network. MIZAN IT owns and operates its own private data center     with a 24/7/365 Support Team that is there to help you at any and every     time. Business hosting is meant for clients that are seeking single   or   multiple domain hosting under one business hosting plan and one   control   panel. Essentially, our fully managed business web hosting   platform   means that we are there to help if there are ever any issues   with your   hosting experience. Whether you call, email or tweet us, or   chat with us   through our Live Chat, we are always there and always   happy to help you   with anything you may need.</p><br>                  
											  <div id="layer">
										<h2 style="color:#A4A5EC; padding-left:10px; font-size:20px;font-width:bold;" align="left">Price List:</h2>
									  <table class="table table-striped"width="600" height="550" cellspacing="0" cellpadding="0" align="left">
										<tbody>
										  <tr>
											<td width="213" valign="top" height="34"><p align="center">Hosting Space</p></td>
											<td width="230" valign="top"><p align="center">Server 1 (Dedicated Server) </p></td>
											<td width="234" valign="top"><p align="center">Server 2 (Shared Server) </p></td>
										  </tr>
										  <tr>
											<td width="213" valign="middle"><p align="center">25 MB</p></td>
											<td width="230" valign="middle"><p align="center">550 Taka / Year or $8/year </p></td>
											<td width="234" valign="middle"><p align="center">100 Taka / Year or $2/year</p></td>
										  </tr>
										  <tr>
											<td width="213" valign="middle"><p align="center">50 MB</p></td>
											<td width="230" valign="middle"><p align="center">1050 Taka / Year or $15/year </p></td>
											<td width="234" valign="middle"><p align="center">200 Taka / Year or $3/year</p></td>
										  </tr>
										  <tr>
											<td width="213" valign="middle"><p align="center">100 MB</p></td>
											<td width="230" valign="middle"><p align="center">2000 Taka / Year or $30/year </p></td>
											<td width="234" valign="middle"><p align="center">400 Taka / Year or $6/year</p></td>
										  </tr>
										  <tr>
											<td width="213" valign="middle"><p align="center">200 MB</p></td>
											<td width="230" valign="middle"><p align="center">3500 Taka / Year or $50/year </p></td>
											<td width="234" valign="middle"><p align="center">700 Taka / Year or $10/year</p></td>
										  </tr>
										  <tr>
											<td width="213" valign="middle"><p align="center">500 MB</p></td>
											<td width="230" valign="middle"><p align="center">6000 Taka / Year or $90/year </p></td>
											<td width="234" valign="middle"><p align="center">1500 Taka / Year or $22/year</p></td>
										  </tr>
										  <tr>
											<td width="213" valign="middle"><p align="center">1 GB</p></td>
											<td width="230" valign="middle"><p align="center">10000 Taka / Year or $150/year </p></td>
											<td width="234" valign="middle"><p align="center">2500 Taka / Year or $35/year</p></td>
										  </tr>
										  <tr>
											<td width="213" valign="middle"><p align="center">2 GB</p></td>
											<td width="230" valign="middle"><p align="center">15000 Taka / Year or $200/year </p></td>
											<td width="234" valign="middle"><p align="center">4000 Taka / Year or $60/year</p></td>
										  </tr>
										  <tr>
											<td width="213" valign="middle"><p align="center">5 GB</p></td>
											<td width="230" valign="middle"><p align="center">25000 Taka / Year or $350/year</p></td>
											<td width="234" valign="middle"><p align="center">7000 Taka / Year or $100/year</p></td>
										  </tr>
										  <tr>
											<td width="213" valign="middle" height="65"><p align="center">10 GB</p></td>
											<td width="230" valign="middle"><p align="center">40000 Taka / Year or $600/year</p></td>
											<td width="234" valign="middle"><p align="center">10000 Taka / Year or $150/year</p></td>
										  </tr>
										  <tr>
											<td width="213" valign="middle"><p align="center">UNLIMITED</p></td>
											<td width="230" valign="middle"><p align="center">80000 Taka / Year or $1200/year</p></td>
											<td width="234" valign="middle"><p align="center">18000 Taka / Year or $250/year</p></td>
										  </tr>
										</tbody>
									  </table>
									  </div>
												<div id="Layer27">
												  <h2 style="color:#1A7FEC; padding-left:10px; font-size:26px text-decoration:underline;" align="justify">Terms of Service for Hosting</h2>
												  <p align="justify"><br>
													<strong>1.) Account Setup / Email on file</strong> <br>
													We will setup your account after we have   received payment and we   and/or our  payment partner(s) have screened the   order(s) in case of   fraud. It is your  responsibility to provide us   with an email address   which is not @ the domain(s)  you are signing up   under. If there is   ever an abuse issue or we need to contact  you, the   primary email   address on file will be used for this purpose. It is  your     responsibility to ensure the email address on file is current or up to      date at all times. If you have a domain name registered with MIZAN IT,     it is  your responsibility to ensure that the contact information for     your domain  account and your actual domain name(s) is correct and     up-to-date. MIZAN IT is  not responsible for a lapsed registration due     to outdated contact information  being associated with the domain. If     you need to verify or change this  information, you should contact our     sales team via email. Providing false  contact information of any kind     may result in the termination of your account.  In dedicated server     purchases or high risk transactions, it will be necessary  to provide     government issued identification and possibly a scan of the credit    card   used for the purchase. If you fail to meet these requirements,   the   order  may be considered fraudulent in nature and be denied.<br>
												  </p>
												  <p align="justify"><strong>Ownership</strong> <br>
													Biller e-mail address which is utilized for   payment on the account   is  designated as the authorized owner of the   account.<br>
												  </p>
												  <p align="justify"><strong>Transfers</strong> <br>
													Our transfer's team will make every effort   to help you move your   site to us.  However, transfers are provided as a   courtesy service,   and we cannot make  guarantees regarding the   availability,   possibility, or time required to  complete an account   transfer. Each   hosting company is configured differently, and  some   hosting platforms   save data in an incompatible or proprietary format,    which may make   it extremely difficult if not impossible to migrate some   or all    account data. We will try our best, but in some cases we may be   unable   to  assist you in a transfer of data from an old host.<br>
												  </p>
												  <p align="justify"><strong>Dedicated  IP Address Allocation</strong> <br>
													Any dedicated IP order in addition to ones   provided with a hosting   package may  be subject to IP Justification.   Justification practices   are subject to change  to remain in conformity   with policies of   American Registry for Internet Numbers  (ARIN). We   reserve the right   to deny any dedicated IP request based on    insufficient justification   or current IP utilization.<br>
												  </p>
												  <p align="justify"><strong>Third  Party Providers</strong> <br>
													All transactions with third party providers   are solely between the   visitor and  the individual provider. MIZAN IT is   not the agent,   representative, trustee or  fiduciary of you or the   third party   provider in any transaction. Some products  provided by   MIZAN IT are   provided under license with vendors, and the use of  any   such third   party products will be governed by the applicable license    agreement,   if any, with such third party.<br>
													All  discounts, promotions and special third   party offers may be   subject to  additional restrictions and limitations   by the third party   provider. All  transactions with third party   providers are subject to   the terms and conditions  under which the   provider agrees with you to   provide the goods or services. You  should   confirm the terms of any   purchase and/or use of goods or services with    the specific provider   with whom you are dealing.<br>
													We  do not make any representations or   warranties regarding, and   are not liable  for, the quality,   availability, or timeliness of goods   or services provided by  a third   party provider. You undertake all   transactions with these providers at    your own risk. We do not warrant   the accuracy or completeness of any    information regarding third   party providers.<br>
												  </p>
												  <p align="justify"><strong>2.) Content</strong> <br>
													All services provided by MIZAN IT may only   be used for lawful   purposes. The  laws of the State of Florida, the   State of Texas, and   the United States of  America apply.<br>
													The  customer agrees to indemnify and hold   harmless MIZAN IT from any claims  resulting from the use of our   services.<br>
												  </p>
												  <p align="justify"><strong>Using  a shared account as a backup/storage device is not permitted</strong>, with the exception of one cPanel backup of the same  account. Please do not take backups of your backups.<br>
													Examples of unacceptable material on  all Shared and Reseller servers include: </p>
												  <div align="justify">
													<ul type="disc">
													  <li>Topsites</li>
													  <li>IRC Scripts/Bots</li>
													  <li>Proxy Scripts/Anonymizers</li>
													  <li>Pirated Software/Warez</li>
													  <li>Image Hosting Scripts (similar       to Photobucket or Tinypic)</li>
													  <li>AutoSurf/PTC/PTS/PPC sites</li>
													  <li>IP Scanners</li>
													  <li>Bruteforce       Programs/Scripts/Applications</li>
													  <li>Mail Bombers/Spam Scripts</li>
													  <li>Banner-Ad services (commercial       banner ad rotation)</li>
													  <li>File Dump/Mirror Scripts       (similar to rapidshare)</li>
													  <li>Commercial Audio Streaming       (more than one or two streams)</li>
													  <li>Escrow/Bank Debentures</li>
													  <li>High-Yield Interest Programs       (HYIP) or Related Sites</li>
													  <li>Investment Sites (FOREX, E-Gold       Exchange, Second Life/Linden Exchange, Ponzi, MLM/Pyramid Scheme)</li>
													  <li>Sale of any controlled       substance without prior proof of appropriate permit(s)</li>
													  <li>Prime Banks Programs</li>
													  <li>Lottery/Gambling Sites</li>
													  <li>MUDs/RPGs/PBBGs</li>
													  <li>Hacker focused       sites/archives/programs</li>
													  <li>Sites promoting illegal       activities</li>
													  <li>Forums and/or websites that       distribute or link to warez/pirated/illegal content</li>
													  <li>Bank Debentures/Bank Debenture       Trading Programs</li>
													  <li>Fraudulent Sites (Including,       but not limited to sites listed at aa419.org &amp; escrow-fraud.com)</li>
													  <li>Push button mail scripts</li>
													  <li>Broadcast or Streaming of Live       Sporting Events (UFC, NASCAR, FIFA, NFL, MLB, NBA, WWE, WWF, etc) </li>
													  <li>Tell A Friend Scripts</li>
													  <li>Anonymous or Bulk SMS Gateways</li>
													  <li>Bitcoin Miners</li>
													  <li>PayDay Loan Sites (including       any site related to PayDay loans, PayDay loan affiliate progams, etc)</li>
													</ul>
												  </div>
												  <p align="justify">Examples  of unacceptable material on all Dedicated servers include:</p>
												  <div align="justify">
													<ul type="disc">
													  <li>IRCD (irc servers)</li>
													  <li>IRC Scripts/Bots</li>
													  <li>Pirated Software/Warez</li>
													  <li>IP Scanners</li>
													  <li>Bruteforce       Programs/Scripts/Applications</li>
													  <li>Mail Bombers/spam Scripts</li>
													  <li>Escrow</li>
													  <li>High-Yield Interest Programs       (HYIP) or Related Sites</li>
													  <li>Investment Sites (FOREX, E-Gold       Exchange, Second Life/Linden Exchange, Ponzi, MLM/Pyramid Scheme)</li>
													  <li>Sale of any controlled       substance without prior proof of appropriate permit(s)</li>
													  <li>Prime Banks Programs</li>
													  <li>Lottery/Gambling Sites</li>
													  <li>Hacker focused       sites/archives/programs</li>
													  <li>Sites promoting illegal       activities</li>
													  <li>Forums and/or websites that       distribute or link to warez/pirated/illegal content</li>
													  <li>Bank Debentures/Bank Debenture       Trading Programs</li>
													  <li>Fraudulent Sites (Including,       but not limited to sites listed at aa419.org &amp; escrow-fraud.com)</li>
													  <li>Mailer Pro</li>
													  <li>Broadcast or Streaming of Live       Sporting Events (UFC, NASCAR, FIFA, NFL, MLB, NBA, WWE, WWF, etc) </li>
													</ul>
												  </div>
												  <p align="justify">MIZAN  IT services, including all related     equipment, networks and network devices are  provided only for     authorized customer use. MIZAN IT systems may be monitored  for all     lawful purposes, including to ensure that use is authorized, for      management of the system, to facilitate protection against unauthorized     access,  and to verify security procedures, survivability, and     operational security.  During monitoring, information may be examined,     recorded, copied and used for  authorized purposes. Use of MIZAN IT     system(s) constitutes consent to  monitoring for these purposes.<br>
													Any  account found connecting to a third   party network or system   without  authorization from the third party is   subject to suspension.   Access to networks  or systems outside of your   direct control must be   with expressed written  consent from the third   party. MIZAN IT may, at   its discretion, request and  require   documentation to prove access to   a third party network or system is    authorized.<br>
													We  reserve the right to refuse service to   anyone. Any material   that, in our  judgment, is obscene, threatening,   illegal, or violates   our terms of service in  any manner may be removed   from our servers   (or otherwise disabled), with or  without notice. <br>
													Failure  to respond to email from our abuse   department within 48   hours may result in the  suspension or termination   of your services.   All abuse issues must be dealt with  via   troubleticket/email and will   have a response within 48 hours.<br>
													Sites  hosted on MIZAN IT.com's service(s)   are regulated only by   U.S. law. Given this  fact, and pursuant to   Section 230(c) of the   Communications Decency Act, we do  not remove   allegedly defamatory   material from domains hosted on our service(s).    The only exception to   this rule is if the material has been found to be   defamatory  by a   court, as evidenced by a court order. MIZAN IT.com is   not in a   position to  investigate and validate or invalidate the   veracity of   individual defamation  claims, which is why we rely on the   legal   system and courts to determine  whether or not material is indeed     considered defamatory. In any case in which a  court order indicates     material is defamatory, libelous, or slanderous in  nature; we will     disable access to the material. Similarly, in any case in which  a US     Court has placed an injunction against specified content or material; we      will comply and remove or disable access to the material in   question. <br>
													The  language of Section 230(c) of the   Communications Decency Act   fundamentally  states that Internet services   providers like MIZAN   IT.com and many of MIZAN IT.com's  other webhosting   services and   brands are republishes and not the publisher of  content.   Our service   merely provides a hosting platform and space on which to    host   content, and any creation or publication of content on our services   is   the  sole responsibility of the third-party user which creates or     publishes the  content. Therefore, MIZAN IT.com should not be held     liable for any allegedly  defamatory, offensive or harassing content     published on sites hosted under MIZAN  IT.com's webhosting service(s). <br>
													If  in doubt regarding the acceptability of   your site or service,   please contact us  at abuse@MIZAN IT.com and we   will be happy to   assist you.<br>
													Potential  harm to minors is strictly   forbidden, including but not   limited to child  pornography or content   perceived to be child   pornography (Lolita):<br>
													Any  site found to host child pornography or   linking to child pornography will be  suspended immediately without   notice.<br>
													Resellers:  we will suspend the site in   question and will notify   you so you may terminate  the account. We will   further monitor your   activity; more than one infraction of  this type   may result in the   immediate termination of your account. <br>
													Direct  customers: Your services will be terminated with or without notice.<br>
													Violations  will be reported to the appropriate law enforcement agency.<br>
													<strong>It  is your responsibility to ensure that scripts/programs     installed under your  account are secure and permissions of directories     are set properly, regardless  of installation method. When at all     possible, set permissions on most directories  to 755 or as restrictive     as possible. Users are ultimately responsible for all  actions taken     under their account. This includes the compromise of credentials  such     as user name and password. It is required that you use a secure     password.  If a weak password is used, your account may be suspended     until you agree to  use a more secure password. Audits may be done to     prevent weak passwords from  being used. If an audit is performed, and     your password is found to be weak, we  will notify you and allow time     for you to change/update your password.</strong> <br>
													<strong>HIPAA  Disclaimer</strong> We are not “HIPAA compliant."<br>
													Users  are solely responsible for any   applicable compliance with   federal or state laws  governing the privacy   and security of personal   data, including medical or other  sensitive   data. Users acknowledge   that the Services may not be appropriate for    the storage or control   of access to sensitive data, such as information   about  children or   medical or health information. MIZAN IT.com does not   control or    monitor the information or data you store on, or transmit   through, our    Services. We specifically disclaim any representation or   warranty   that the  Services, as offered, comply with the federal Health     Insurance Portability and  Accountability Act (“HIPAA”). Customers     requiring secure storage of “protected  health information” under HIPAA     are expressly prohibited from using this  Service for such purposes.     Storing and permitting access to “protected health  information,” as     defined under HIPAA is a material violation of this Terms of  Service,     and grounds for immediate account termination. We do not sign    “Business   Associate Agreements” and you agree that MIZAN IT is not a   Business    Associate or subcontractor or agent of yours pursuant to   HIPAA. If you   have  questions about the security of your data, you   should contact <u><a href="mailto:sales@zaman-it.com">sales@mizan-it.com</a></u><br>
												  </p>
												  <p align="justify"><strong>3.) Zero Tolerance Spam Policy</strong><br>
													We take a zero tolerance stance against   sending of unsolicited   e-mail, bulk  emailing, and spam. "Safe lists",   purchased lists, and   selling of  lists will be treated as spam. Any user   who sends out spam   will have their  account terminated with or without   notice.<br>
													Sites  advertised via SPAM (Spamvertised)   may not be hosted on our   servers. This  provision includes, but is not   limited to SPAM sent   via fax, phone, postal  mail, email, instant   messaging, or   usenet/newsgroups. No organization or entity  listed in   the Rokso may      be hosted on our servers. Any account which results in our IP space     being  blacklisted will be immediately suspended and/or terminated.<br>
													MIZAN  IT reserves the right to require   changes or disable as   necessary any web site,  account, database, or   other component that   does not comply with its established  policies, or   to make any such   modifications in an emergency at its sole  discretion.<br>
													MIZAN  IT reserves the right to charge the   holder of the account   used to send any  unsolicited e-mail a clean up   fee or any charges   incurred for blacklist  removal. This cost of the   clean up fee is   entirely at the discretion of MIZAN  IT.<br>
												  </p>
												  <p align="justify"><strong>4.) Payment Information</strong> <br>
													You  agree to supply appropriate payment for   the services received   from MIZAN IT, in  advance of the time period   during which such   services are provided. Subject to  all applicable   laws, rules, and   regulations, all payments will apply to the  oldest   invoice(s) in your   billing account. You agree that until and unless you    notify MIZAN IT   of your desire to cancel any or all services received,   those    services will be billed on a recurring basis.<br>
													As  a client of MIZAN IT, it is your   responsibility to ensure that   your payment  information is up to date,   and that all invoices are   paid on time. You agree  that until and unless   you notify MIZAN IT of   your desire to cancel any or all  services   received (by the proper   means listed in the appropriate section of the    Terms of Service),   those services will be billed on a recurring basis,   unless  otherwise   stated in writing by MIZAN IT. MIZAN IT reserves the   right to bill    your credit card or billing information on file with us.   MIZAN IT   provides a 10  day grace period from the time the invoice is   generated   and when it must be  paid. Any invoice that is outstanding for   10   days and not paid will result in a  $10 late fee and/or an account     suspension until account balance has been paid  in full. The $10 late     fee is applied in addition to whatever else is owed to MIZAN  IT for     services rendered. Access to the account will not be restored until      payment has been received.<br>
													MIZAN  IT reserves the right to change the monthly payment amount and any other  charges at anytime.<br>
												  </p>
												  <p align="justify"><strong>5.) Backups and Data Loss</strong><br>
													Your use of this service is at your sole   risk. Our backup service   runs once a  week, overwrites any of our   previous backups made, and   only one week of backups  are kept. This   service is provided to you as   a courtesy. MIZAN IT is not  responsible   for files and/or data   residing on your account. You agree to take  full   responsibility for   files and data transferred and to maintain all    appropriate backup of   files and data stored on MIZAN IT servers. <br>
												  </p>
												  <p align="justify"><strong>6.) Cancellations and Refunds</strong><br>
													MIZAN IT reserves the right to cancel,   suspend, or otherwise   restrict access to  the account at any time with   or without notice.<br>
													Exchange  rate fluctuations for   international payments are   constant and unavoidable. All  refunds are   processed in U.S. dollars,   and will reflect the exchange rate in  effect   on the date of the   refund. All refunds are subject to this fluctuation    and MIZAN IT is   not responsible for any change in exchange rates   between time  of   payment and time of refund.<br>
													Cancellations  requested after the initial   45 days for Shared and   Reseller accounts will go  into effect on the   renewal date for that   particular hosting package. Unless  specifically   requested, the   account will remain active until the period  expires. If   the account   is eligible, any request for a refund outside of the    initial 45 day   period will be given on a prorated basis with any   previous  extended   term discount withheld. Refunds are to be determined   once the account    has been canceled. Payments older than 60 days may   require a refund   via PayPal  or mailed check due to our merchant account   policies and   procedures.<br>
													The  following methods of payments are   non-refundable, and refunds will be posted as  credit to the hosting   account:</p>
												  <div align="justify">
													<ul type="disc">
													  <li>Bank Wire Transfers</li>
													  <li>Western Union Payments</li>
													  <li>Checks</li>
													  <li>Money orders</li>
													</ul>
												  </div>
												  <p align="justify">There  are no refunds on dedicated servers,     administrative fees, and install fees for  custom software. Refund     requests for .com, .net., and .org domain names (made  within the 45 day     money-back guarantee period) will have the common market  value     subtracted for those tld's. Any ccTLD's domain name purchases are      non-refundable. Please note that domain refunds will only be considered     if they  were ordered in conjunction with a hosting package.   Eligibility   of said refunds  will be determined at the time of   cancellation.<br>
													Only  first-time accounts are eligible for a   refund. For example,   if you've had an  account with us before, canceled   and signed up   again, you will not be eligible  for a refund or if you   have opened a   second account with us.<br>
													<strong>Violations  of the Terms of Service will waive the refund policy.</strong> <br>
												  </p>
												  <p align="justify"><strong>7a.) Resource Usage</strong> <br>
													User  may not: <br>
													1) Use 25% or more of system resources for   longer then 90 seconds.   There are  numerous activities that could cause   such problems; these   include: CGI scripts,  FTP, PHP, HTTP, etc.<br>
													2) Run stand-alone, unattended server-side   processes at any point   in time on  the server. This includes any and all   daemons, such as   IRCD. <br>
													3) Run any type of web spider or indexer (including Google Cash / AdSpy) on  shared servers.<br>
													4) Run any software that interfaces with an IRC (Internet Relay Chat) network.<br>
													5) Run any bit torrent application, tracker,   or client. You may   link to legal  torrents off-site, but may not host   or store them on   our shared servers. <br>
													6) Participate in any file-sharing/peer-to-peer activities<br>
													7) Run any gaming servers such as counter-strike, half-life, battlefield1942,  etc <br>
													8) Run cron entries with intervals of less than 15 minutes.<br>
													9) Run any MySQL queries longer than 15 seconds. MySQL tables should be indexed  appropriately.<br>
													10) When using PHP include functions for   including a local file,   include the  local file rather than the URL.   Instead of    include("http://yourdomain.com/include.php") use      include("include.php")<br>
													11) To help reduce usage, do not force html to handle server-side code (like  php and shtml).<br>
													12) Only use https protocol when necessary;   encrypting and   decrypting  communications is noticeably more   CPU-intensive than   unencrypted  communications.<br>
												  </p>
												  <p align="justify"><strong>7b.) INODES </strong><br>
													The use of more than 250,000 inodes on any   shared account may   potentially  result in a warning first, and if no   action is taken   future suspension.  Accounts found to be exceeding the   100,000 inode   limit will automatically be  removed from our backup   system to avoid   over-usage, however databases will  still be backed up.   Every file (a   webpage, image file, email, etc) on your  account uses up 1   inode.<br>
													Sites  that slightly exceed our inode limits   are unlikely to be   suspended; however,  accounts that constantly create   and delete large   numbers of files on a regular  basis, have hundreds of   thousands of   files, or cause file system damage may be  flagged for   review and/or   suspension. The primary cause of excessive inodes  seems   to be due to   users leaving their catchall address enabled, but never    checking   their primary account mailbox. Over time, tens of thousands of      messages (or more) build up, eventually pushing the account past our     inode  limit. To disable your default mailbox, login to cPanel and     choose  "Mail", then "Default Address", "Set Default  Address", and then     type in: :fail: No such user here.<br>
												  </p>
												  <p align="justify"><strong>7c.) Backup Limit </strong><br>
													Any shared account using more than 20 gigs   of disk space will be   removed from  our off site weekly backup with the   exception of   Databases continuing to be  backed up. All data will   continue to be   mirrored to a secondary drive which  helps protect   against data loss   in the event of a drive failure. <br>
												  </p>
												  <p align="justify"><strong>8.) Bandwidth Usage</strong><br>
													You are allocated a monthly bandwidth   allowance. This allowance   varies  depending on the hosting package you   purchase. Should your   account pass the  allocated amount we reserve the   right to suspend the   account until the start of  the next allocation,   suspend the account   until more bandwidth is purchased at  an additional   fee, suspend the   account until you upgrade to a higher level of    package, terminate the   account and/or charge you an additional fee for   the  overages. Unused   transfer in one month cannot be carried over to   the next  month.<br>
												  </p>
												  <p align="justify"><strong>9.) Uptime Guarantee</strong><br>
													If your shared / reseller server has a   physical downtime that is   not within the  99.9% uptime you may receive   one month of credit on   your account. Approval of  the credit is at the   discretion of MIZAN IT   dependent upon justification  provided. Third   party monitoring   service reports may not be used for  justification due   to a variety of   factors including the monitor's network    capacity/transit   availability. The uptime of the server is defined as   the  reported   uptime from the operating system and the Apache Web Server   which may    differ from the uptime reported by other individual   services. <br>
												  </p>
												  <p align="justify"><strong>10.)  Reseller: Client Responsibility</strong><br>
													Resellers are responsible for supporting   their clients. MIZAN IT   does not  provide support to our Reseller's   Clients. If a reseller's   client contacts us,  we reserve the right to   place the client account   on hold until the reseller can  assume their   responsibility for their   client. All support requests must be made  by   the reseller on their   clients' behalf for security purposes. Resellers   are  also responsible   for all content stored or transmitted under their   reseller  account   and the actions of their clients'. MIZAN IT will hold   any reseller    responsible for any of their clients actions that violate   the law or   the terms  of service.<br>
												  </p>
												  <p align="justify"><strong>11.) Shared (non-reseller accounts) / Semidedicated Servers</strong><br>
													Shared accounts may not resell web hosting   to other people, if you   wish to  resell hosting you must use a reseller   account.   Semi-dedicated servers are not  backed up. You must maintain   your own   backups.<br>
													The  Business plan provides a free toll-free   number for your web   site and business.  Minutes Included Per Month: 100   -- Additional   Minutes: 4.9 Cents. Included  minutes are based on a   calendar month.   By default, you'll be able to receive  calls from US   callers only. You   can enable calls from Canada in your control  panel,   but will be   billed 6.9 cents per minute for all calls from Canada due to    the   increased costs of providing service there. This service is   provided   and  supported by Voipo for new MIZAN IT clients.<br>
													*Please  note: Toll-Free numbers issued must   be called at least   once every 30 days.  Toll-free numbers with 0 calls   in a 30 day period   will be considered inactive  and disconnected. If you   cancel your   hosting package with MIZAN IT or switch to  a plan other   than the   business plan, your toll-free number will be disconnected    unless you   contact VOIPo to change to a paid service plan prior to the   MIZAN IT    cancellation. <br>
												  </p>
												  <p align="justify"><strong>12.) Dedicated Servers</strong><br>
													MIZAN IT reserves the right to reset the   password on a dedicated   server if the  password on file is not current   so that we may do   security audits as required  by our datacenter. It is   the   responsibility of the client to ensure that there  is a valid email     address and current root password on file for their dedicated  server on     file to prevent downtime from forced password resets. MIZAN IT      reserves the right to audit servers as needed and to perform     administrative  actions at the request of our datacenter. Dedicated     servers are NOT backed up  by us and it is the responsibility of the     client to maintain backups or have a  solution for this. It is your     responsibility to maintain backups.<br>
												  </p>
												  <p align="justify"><strong>13.) Price Change</strong><br>
													The amount you pay for hosting will never   increase from the date   of purchase.  We reserve the right to change   prices listed on MIZAN   IT.com, and the right to  increase the amount of   resources given to   plans at any time.<br>
												  </p>
												  <p align="justify"><strong>14.) Coupons</strong><br>
													Discounts and coupon codes are reserved for   first-time accounts   *or first-time  customers* only and may not be used   towards the   purchase of a domain  registration unless otherwise   specified. If you   have signed up using a  particular domain, you may not   resign up for   that domain using another coupon  at a later date. Any   account found   in violation of these policies will be  reviewed by our   sales   department and the appropriate invoices will be added to  the   account.   Coupon abuse will not be tolerated and may result in the    suspension   or termination of the account. Coupons or discounts are only   valid    towards the initial purchase, and do not affect the renewal or     recurring price. <br>
												  </p>
												  <p align="justify"><strong>15a.) Indemnification</strong><br>
													Customer agrees that it shall defend,   indemnify, save and hold   MIZAN IT  harmless from any and all demands,   liabilities, losses,   costs and claims,  including reasonable attorney's   fees asserted   against MIZAN IT, its agents, its  customers, officers and   employees,   that may arise or result from any service  provided or   performed or   agreed to be performed or any product sold by  customer,   its agents,   employees or assigns. Customer agrees to defend,  indemnify   and hold   harmless MIZAN IT against liabilities arising out of; (1)  any   injury   to person or property caused by any products sold or otherwise      distributed in connection with MIZAN IT; (2) any material supplied by     customer  infringing or allegedly infringing on the proprietary rights     of a third party;  (3) copyright infringement and (4) any defective     products sold to customers  from MIZAN IT's server.<br>
												  </p>
												  <p align="justify"><strong>15b.) Arbitration</strong> <br>
													By using any MIZAN IT services, you agree to   submit to binding   arbitration. If  any disputes or claims arise against   MIZAN IT or its   subsidiaries, such  disputes will be handled by an   arbitrator of MIZAN   IT's choice. An arbitrator  from the American   Arbitration Association   or the National Arbitration Forum  will be   selected in the state of   Texas. Arbitrators shall be attorneys or    retired judges and shall be   selected pursuant to the applicable rules.   All  decisions rendered by   the arbitrator will be binding and final. The    arbitrator's award is   final and binding on all parties. The Federal   Arbitration  Act, and   not any state arbitration law, governs all   arbitration under this    Arbitration Clause. You are also responsible for   any and all costs   related to  such arbitration. <br>
												  </p>
												  <p align="justify"><strong>16.) Disclaimer</strong><br>
													MIZAN IT shall not be responsible for any   damages your business   may suffer. MIZAN  IT makes no warranties of any   kind, expressed or   implied for services we  provide. MIZAN IT disclaims   any warranty or   merchantability or fitness for a  particular purpose.   This includes   loss of data resulting from delays, no  deliveries, wrong   delivery,   and any and all service interruptions caused by MIZAN  IT and   its   employees.<br>
												  </p>
												  <p align="justify"><strong>17.) Disclosure to law enforcement</strong><br>
													MIZAN IT may disclose any subscriber   information to law   enforcement agencies  without further consent or   notification to the   subscriber upon lawful request  from such agencies.   We will cooperate   fully with law enforcement agencies. <br>
												  </p>
												  <p align="justify"><strong>18.) Changes to the TOR</strong><br>
													MIZAN IT reserves the right to revise its policies at any time without notice.</p>
											  </div>
												<p align="justify">&nbsp;</p>
												<h4>&nbsp;</h4>
											</div>
								 
								  <div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									
								  </div>
								</div>
							  </div>
							</div>
						<h4 class="text-center" style="color:#F02D28;text-width:bold;">Hosting Server </h4>
				</figure>
				
				<figure class="mix work-item Web_Design">
					<a class="service1" href="#Web_Design" data-toggle="modal" data-target="#Web_Design"rel="works" title="Write Your Image Caption Here"><h3>View Details</h3><img src="img/works/item-7.jpg" alt=""></a>
		 
							<!-- Modal -->
							<div class="modal fade" id="Web_Design" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							  <div class="modal-dialog" role="document">
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h2 style="color:#1A7FEC; font-width:bold;" class="modal-title" id="myModalLabel">Web Design & Deveopment</h2>
								  </div>
										 <div id="tooplate_content">
										  <div id="Layer27">
											<p align="justify"><strong>Static Website</strong></p>
											<p align="justify">This is a starter business   packages. We will   design high quality website for your business. With   this package you   can not add delete or modify your website content by   yourself. If you   need to make any changes, you need to refer to us. </p>
											<p align="justify"><strong>Dynamic Website </strong></p>
											<p align="justify">Dynamic will invite your site   visitors to become   involved and engaged. Dynamic sites on the other hand   can be more   expensive to develop initially, but the advantages are   numerous. At a   basic level, a dynamic website can give the website owner   the ability   to simply update and add new content to the site. For   example, news   and events could be posted to the site through a simple   browser   interface.</p>
											<p align="justify"><strong>Ecommerce Website</strong></p>
											<p align="justify">E-commerce website is for them who   wants to sell   their product online. We design E-commerce website for you   according   to your requirements. You can directly send your products   from your   website to Ebay, Facebook &amp; Google base . User can buy   your   products using debit and credit card. </p>
										  </div>
										  <h4>&nbsp;</h4>
										</div>
								  <div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									
								  </div>
								</div>
							  </div>
							</div>
						<h4 class="text-center" style="color:#F02D28;text-width:bold;">Web_Design & Deveopment </h4>
				</figure>
				
				<figure class="mix work-item E_Commerce">
					<a class="service1" href="#E_Commerce" data-toggle="modal" data-target="#E_Commerce"rel="works" title="Write Your Image Caption Here"><h3>View Details</h3><img src="img/works/item-2.jpg" alt=""></a>
		 
							<!-- Modal -->
							<div class="modal fade" id="E_Commerce" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							  <div class="modal-dialog" role="document">
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h3 style="color:#1A7FEC; font-width:bold;" class="modal-title" id="myModalLabel">Website design and development package (Ecommerce)</h3>
								  </div>
								  <div id="tooplate_content">
										
										  <div id="Layer27">
											<div id="Layer7">
											  <div align="justify"><strong><u>Ecommerce Package - 1 (70,000 taka / $1,000):</u></strong>
												<ul>
												  <li>Maximum Product: 50</li>
												  <li>Max Product Category: 5</li>
												  <li>Dynamic Page: 10</li>
												  <li>Design Concept sample:01</li>
												  <li>Flash Banner option with 3 Banner picture</li>
												  <li>Menu / POP Up Menu: 10</li>
												  <li>Shopping Cart</li>
												  <li>Clients registration and payment option</li>
												  <li>Shipment option</li>
												  <li>Order management option</li>
												  <li>Auto generated bill</li>
												  <li>Research &amp; Analysis: 03 Hour</li>
												</ul>
											  </div>
											  <p align="justify"><strong>Domain &amp; Hosting:</strong></p>
											  <div align="justify">
												<ul>
												  <li>Your own Domain gTLD .com .net .org .info Registration</li>
												  <li>1000 MB Hosting under Linux Server for one year.</li>
												  <li>50 Own matching Email address (you@yourname.com)</li>
												  <li>10000 MB Monthly Data transfer / Traffic.</li>
												  <li>100+ Add-on and Sub domains.</li>
												  <li>Unlimited My-Sql Database.</li>
												  <li>Apache 2.2.x PHP 5.x , Mysql 5.x</li>
												  <li>FrontPage Server Extensions with AwStats/Webalizer.</li>
												  <li>Linux Based Hosting server with FTP and cPanel.</li>
												  <li>All Other Standard Hosting facilities with Control Panel.</li>
												</ul>
											  </div>
											  <p align="justify"><strong>Terms &amp; Condition:</strong></p>
											  <div align="justify">
												<ul>
												  <li>Development Timelines (If content is available): 45 working days</li>
												  <li>If Client Failed to provide the content within 30 days after contract then the project will count as a complete project.</li>
												  <li>Post Sales Support: 1 month</li>
												  <li>Charges per extra product: 1500 taka / $22</li>
												  <li><strong>Renew: 20,000 taka / $280 per year for Domain &amp; Hosting</strong></li>
												  <li>Domain and hosting must be renewing in every year before 1     month of expired date. Otherwise additional cost will be added     according to the domain hosting renew rules.</li>
												  <li>Iimage need to customized like background change or   color   change or any kind of editing then additional cost will be 50     taka/$0.70 per picture</li>
												</ul>
											  </div>
											  <p align="justify"><strong><u>Ecommerce Package - 2 (1,00,000 taka / $1500):</u></strong></p>
											  <div align="justify">
												<ul>
												  <li>Maximum Product: 100</li>
												  <li>Max Product Category: 10</li>
												  <li>Maximum Dynamic Page: 15</li>
												  <li>Design Concept sample:01</li>
												  <li>Flash Banner option with 3 Banner picture</li>
												  <li>Menu / POP Up Menu: 15</li>
												  <li>Shopping Cart</li>
												  <li>Clients registration and payment option</li>
												  <li>Shipment option</li>
												  <li>Order management option</li>
												  <li>Auto generated bill</li>
												  <li>Research &amp; Analysis: 03 Hour</li>
												</ul>
											  </div>
											  <p align="justify"><strong>Domain &amp; Hosting:</strong></p>
											  <div align="justify">
												<ul>
												  <li>Your own Domain gTLD .com .net .org .info Registration</li>
												  <li>1000 MB Hosting under Linux Server for one year.</li>
												  <li>50 Own matching Email address (you@yourname.com)</li>
												  <li>10000 MB Monthly Data transfer / Traffic.</li>
												  <li>100+ Add-on and Sub domains.</li>
												  <li>Unlimited My-Sql Database.</li>
												  <li>Apache 2.2.x PHP 5.x , Mysql 5.x</li>
												  <li>FrontPage Server Extensions with AwStats/Webalizer.</li>
												  <li>Linux Based Hosting server with FTP and cPanel.</li>
												  <li>All Other Standard Hosting facilities with Control Panel.</li>
												</ul>
											  </div>
											  <p align="justify"><strong>Terms &amp; Condition:</strong></p>
											  <div align="justify">
												<ul>
												  <li>Development Timelines (If content is available): 45 working days</li>
												  <li>If Client Failed to provide the content within 30 days after contract then the project will count as a complete project.</li>
												  <li>Post Sales Support: 1 month</li>
												  <li>Charges per extra product: 1500 taka / $22</li>
												  <li><strong>Renew: 20,000 taka / $280 per year for Domain &amp; Hosting</strong></li>
												  <li>Domain and hosting must be renewing in every year before 1     month of expired date. Otherwise additional cost will be added     according to the domain hosting renew rules. </li>
												  <li>If image need to customized like background change or   color   change or any kind of editing then additional cost will be 50     taka/$0.70 per pictu</li>
												</ul>
											  </div>
											  <p align="justify"><strong><u>Ecommerce Package - 3 (1,50,000 taka):</u></strong></p>
											  <div align="justify">
												<ul>
												  <li>Maximum Product: 500</li>
												  <li>Max Product Category: 50</li>
												  <li>Dynamic Page: 25</li>
												  <li>Design Concept sample:01</li>
												  <li>Flash Banner option with 3 Banner picture</li>
												  <li>Menu / POP Up Menu: 25</li>
												  <li>Shopping Cart</li>
												  <li>Clients registration and payment option</li>
												  <li>Shipment option</li>
												  <li>Order management option</li>
												  <li>Auto generated bill</li>
												  <li>Research &amp; Analysis: 03 Hour</li>
												</ul>
											  </div>
											  <p align="justify"><strong>Domain &amp; Hosting:</strong></p>
											  <div align="justify">
												<ul>
												  <li>Your own Domain gTLD .com .net .org .info Registration</li>
												  <li>1000 MB Hosting under Linux Server for one year.</li>
												  <li>50 Own matching Email address (you@yourname.com)</li>
												  <li>10000 MB Monthly Data transfer / Traffic.</li>
												  <li>100+ Add-on and Sub domains.</li>
												  <li>Unlimited My-Sql Database.</li>
												  <li>Apache 2.2.x PHP 5.x , Mysql 5.x</li>
												  <li>FrontPage Server Extensions with AwStats/Webalizer.</li>
												  <li>Linux Based Hosting server with FTP and cPanel.</li>
												  <li>All Other Standard Hosting facilities with Control Panel.</li>
												</ul>
											  </div>
											  <p align="justify"><strong>Terms &amp; Condition:</strong></p>
											  <div align="justify">
												<ul>
												  <li>Development Timelines (If content is available): 45 working days</li>
												  <li>If Client Failed to provide the content within 30 days after contract then the project will count as a complete project.</li>
												  <li>Post Sales Support: 1 month</li>
												  <li>Charges per extra product: 1500 taka / $22</li>
												  <li><strong>Renew: 20,000 taka / $280 per year for Domain &amp; Hosting</strong></li>
												  <li>Domain and hosting must be renewing in every year before 1     month of expired date. Otherwise additional cost will be added     according to the domain hosting renew rules.</li>
												  <li>If image need to customized like background change or   color   change or any kind of editing then additional cost will be 50     taka/$0.70 per picture.</li>
												</ul>
											  </div>
											  <p align="justify"><strong><u>&nbsp;</u></strong></p>
											  <p align="justify"><strong><u>Ecommerce Package - Unlimited (2,00,000 taka):</u></strong></p>
											  <div align="justify">
												<ul>
												  <li>Maximum Product: Unlimited</li>
												  <li>Max Product Category: Unlimited</li>
												  <li>Dynamic Page: Unlimited</li>
												  <li>Design Concept sample:01</li>
												  <li>Flash Banner option with 3 Banner picture</li>
												  <li>Menu / POP Up Menu: Unlimited</li>
												  <li>Shopping Cart</li>
												  <li>Clients registration and payment option</li>
												  <li>Shipment option</li>
												  <li>Order management option</li>
												  <li>Auto generated bill</li>
												  <li>Research &amp; Analysis: 09 Hour</li>
												</ul>
											  </div>
											  <p align="justify"><strong>Domain &amp; Hosting:</strong></p>
											  <div align="justify">
												<ul>
												  <li>Your own Domain gTLD .com .net .org .info Registration</li>
												  <li>5000 MB Hosting under Linux Server for one year.</li>
												  <li>50 Own matching Email address (you@yourname.com)</li>
												  <li>50000 MB Monthly Data transfer / Traffic.</li>
												  <li>100+ Add-on and Sub domains.</li>
												  <li>Unlimited My-Sql Database.</li>
												  <li>Apache 2.2.x PHP 5.x , Mysql 5.x</li>
												  <li>FrontPage Server Extensions with AwStats/Webalizer.</li>
												  <li>Linux Based Hosting server with FTP and cPanel.</li>
												  <li>All Other Standard Hosting facilities with Control Panel.</li>
												</ul>
											  </div>
											  <p align="justify"><strong>Terms &amp; Condition:</strong></p>
											  <div align="justify">
												<ul>
												  <li>Development Timelines (If content is available): 45 working days</li>
												  <li>If Client Failed to provide the content within 30 days after contract then the project will count as a complete project.</li>
												  <li>Post Sales Support: 1 month</li>
												  <li>Charges per extra product: 1500 taka / $22</li>
												  <li><strong>Renew: 20,000 taka / $280 per year for Domain &amp; Hosting</strong></li>
												  <li>Domain and hosting must be renewing in every year before 1     month of expired date. Otherwise additional cost will be added     according to the domain hosting renew rules. </li>
												  <li>If image need to customized like background change or   color   change or any kind of editing then additional cost will be 50     taka/$0.70 per pictur</li>
												</ul>
											  </div>
											  <p align="justify"><strong><u>Individual Web Module Price:</u></strong></p>
											  <div align="justify">
												<ul>
												  <li>Online Feedback Form: 2000 taka / $25</li>
												  <li>Visitor Counter: 1000 taka/$150</li>
												  <li>Language Module(Without Bangla): 2000 taka / $25, Bangla &ndash; 1000 taka per page/$15</li>
												  <li>Every A4 size Content: 1500 taka / $20</li>
												  <li>Extra Royalty Free Image: 100 taka per image / $1.5</li>
												  <li>Extra scanned image: 20 taka per image / $0.3</li>
												  <li>Editing Image: 50 taka per image / $0.7</li>
												  <li>Flash Intro: 5000 taka / $70</li>
												  <li>Flash banner: 2000 taka / $25</li>
												  <li>Music: 2000 taka / $25</li>
												  <li>Weather: 3000 taka / $40</li>
												  <li>Google Map: 2000 taka / $25</li>
												</ul>
											  </div>
											  <p align="justify"><strong><u>Support Charge (Within 48 hour support):</u></strong></p>
											  <div align="justify">
												<ul>
												  <li>Design Change Support: 8,000 taka / $100 per design</li>
												  <li>Content Change Support: 2000 taka / $25 per month(Max 2 times)</li>
												  <li>Content Change Support: 4000 taka / $ 50 per month(Max 5 times)</li>
												  <li>Content Change Support: 8000 taka / $100 per month(Max 12 times)</li>
												  <li>Content Change Support: 25000 taka / $330 per month(Unlimited times 10-6 pm)</li>
												</ul>
											  </div>
											  <p align="justify"><strong><u>Training:</u></strong></p>
											  <div align="justify">
												<ul>
												  <li>1st Training(1 hour): Free</li>
												  <li>2nd Training: 2000 taka / $25 per hour (Inside MIZAN IT)</li>
												  <li>Training: 5000 taka / $70 per hour (Outside MIZAN IT)-After 1st training, if needed.</li>
												</ul>
											  </div>
											  <p align="justify">&nbsp;<strong><u>Payment Procedure:</u></strong></p>
											  <div align="justify">
												<ul type="disc">
												  <li>50% advance payment for website development</li>
												  <li>50% due payment within 3 days after complete the full project.</li>
												  <li>Client must provide all content with work order.</li>
												  <li>Payment will goes to “Mizan IT” account pay check</li>
												</ul>
											  </div>
											  <p align="justify"><strong><u>Features:</u></strong></p>
											  <div align="justify"><br>
												<strong>Table of Contents</strong>
												<ul>
												  <li>Document Purpose</li>
												  <li>Client Brief</li>
												  <li>Objective</li>
												  <li>Scope of Work</li>
												  <li>Administrative Panel</li>
												  <li>Standardizations</li>
												  <li>Delivery Schedule</li>
												  <li>Process Flow</li>
												  <li>Requirements from Client</li>
												  <li>MIZAN IT’ Commitment</li>
												  <li>Location of Work</li>
												  <li>Progress Reporting and Communication</li>
												  <li>Engagement Model</li>
												</ul>
											  </div>
											  <p align="justify"><strong>1. Document Purpose</strong><br>
												The copyright of this document rests with MIZAN IT Technologies     Pvt. Ltd (hereafter referred to as MIZAN IT) and no part of the same     should be copied without consulting with the same.<br>
												This is the proposal document for MIZAN IT service offering in   the   website design and development and custom web application   development   space. The document details our understanding of the brief,   the   objectives of the services suite, the methodology, deliverables and     commercials.</p>
											  <p align="justify"><strong>2. Client Brief</strong><br>
												Client desires to develop an ecommerce website</p>
											  <div align="justify">
												<ul>
												  <li>Customers will be able to login/register into the website.</li>
												  <li>Customers will be able to create an account after submitting their email id, name, address, etc on the website.</li>
												  <li>Customers will also be able to easily search for products   by   using different keywords like name, category wise etc and will be     able to refine their results by using filters such as price, product     type etc. on the website.</li>
												  <li>Customers will be able to view the products with details, images, zoom in option etc. on the website.</li>
												  <li>Customers will have the ability to customize their products by submitting the information on the website.</li>
												  <li>Customers can view the events posted by the admin on the website.</li>
												  <li>Customers can submit their reviews on the products listed on the website.</li>
												  <li>Customers will be able to place orders on the website.</li>
												  <li>Customers will be able to check their order status on the website.</li>
												  <li>Customers will be able to use the facility of shopping cart on the website.</li>
												  <li>Customers will be able to make payments for their orders by using integrated payment gateway given by the Client.</li>
												  <li>Customers will be able to choose their mode of shipping using integrated shipping gateway on the website.</li>
												  <li>Customers will be able to view the shipping details on the website.</li>
												  <li>Customers will be able to provide delivery address for each order made on the website.</li>
												  <li>Customers will be able to receive an email for confirmation after an order placed on the website.</li>
												  <li>Customers will be able to receive newsletter after     submitting their name and email id on the website and this section will     be managed by admin.</li>
												  <li>Customers will be able to share the link of the website on the social networking website like facebook, twitter etc.</li>
												  <li>Customers will be able to view FAQ on the website.</li>
												  <li>Admin will be able to manage the customers, products, orders etc on the website from the backend.</li>
												  <li>Provide the basic pages (i.e., about us, Contact Us, FAQ, help) for company information.</li>
												</ul>
											  </div>
											  <p align="justify"><strong>3. Objective</strong><br>
												MIZAN IT focuses on highly qualitative, timely delivered and     cost-effective offshore e-Business Solutions development services. With a     rich and varied experience in providing software development, project     management capabilities and stringent quality standards ensure to     develop solutions that give your business an edge over your competitors.     We are experts at developing and implementing applications for     mission-critical and enterprise- wide projects. Our dedicated team at     MIZAN IT strategically approaches your challenges to develop solutions     and system administration services that meet your objectives - both     short and long term. We specialize in E-Business Solutions wherein we     undertake designing, development, maintenance and promotion of Web Sites     coupled with any other Web Applications. With our resource pool of     experienced professionals coupled with state-of-the-art technology and     industry best practices, it is our vision to make our customers the   best   in the industry offering best of the breed solutions.</p>
											  <p align="justify"><strong>4. Scope of Work</strong><br>
												<strong>Page Templates</strong></p>
											  <div align="justify">
												<ul>
												  <li>Design Templates ( No Cap on the number of revisions)</li>
												  <li>User Visibility research and Layout Engineering</li>
												  <li>Logos ( No Cap on the number of revisions)</li>
												</ul>
											  </div>
											  <p align="justify"><strong>Template Layout</strong><br>
												<strong>Banner</strong></p>
											  <div align="justify">
												<ul>
												  <li>Size according to specifications</li>
												  <li>Logo Placement</li>
												  <li>Text</li>
												  <li>Graphics</li>
												</ul>
											  </div>
											  <p align="justify"><strong>User Visibility Content</strong></p>
											  <div align="justify">
												<ul>
												  <li>Information bar</li>
												  <li>Menu Bar</li>
												  <li>Tool bar</li>
												  <li>Side Bar</li>
												  <li>Header and Footer</li>
												  <li>Text and Graphics</li>
												</ul>
											  </div>
											  <p align="justify"><strong>Design Specifications</strong><br>
												Design tools and technologies The design and layout of the     application will be SEO friendly constructed using CSS and XHTML, DHTML     along with use of AJAX and keeping in mind the latest web 2.0 trends.     The application upon completion will be integrated with Google   analytics   for keeping an eye on the statistics of the site. It will   carry tell a   friend and printer friendly version at all pages. To help   people   bookmark the site easily; add this widget will be embedded at   the top of   the homepage.<br>
												<strong>Front End</strong><br>
												The front end will have following features:-<br>
												<strong><em>Header Pages</em></strong></p>
											  <div align="justify">
												<ul>
												  <li>Home</li>
												  <li>Login/Sign up</li>
												  <li>Search</li>
												  <li>About Us</li>
												</ul>
											  </div>
											  <p align="justify"><strong><em>Footer Pages</em></strong></p>
											  <div align="justify">
												<ul>
												  <li>Contact Us</li>
												  <li>Sitemap</li>
												  <li>Terms and Conditions</li>
												  <li>Privacy Policy</li>
												  <li>FAQ</li>
												</ul>
											  </div>
											  <p align="justify"><strong><em>Website Content Page</em></strong></p>
											  <div align="justify">
												<ul>
												  <li>Home</li>
												  <li>My Account Control Panel for Customers</li>
												  <li>Search</li>
												  <li>Advanced Search</li>
												  <li>Products</li>
												  <li>Products Catalog</li>
												  <li>Products Information</li>
												  <li>Customize</li>
												  <li>Reviews</li>
												  <li>Events</li>
												  <li>Shopping Cart</li>
												  <li>Check Out</li>
												  <li>Shipping</li>
												  <li>Payments</li>
												  <li>Social Media Integration</li>
												  <li>Newsletters</li>
												  <li>Contact Us</li>
												  <li>FAQ</li>
												</ul>
											  </div>
											  <p align="justify"><strong>Customer Registration</strong><br>
												This is the section where customer will be able to register t o     the site as member. Once customer shows interest and wants to get an     account then he will be taken to a page where he will be asked to submit     a form that would have various fields for the customer to enter their     personal details creating a profile of their own. They will be able   to   submit captcha at the time of registration. This customer will     automatically be assigned the account manager role for this particular     account.<br>
												<strong>Existing Customer</strong><br>
												After the account is activated the customer will be able to perform the following basic tasks in account settings:</p>
											  <div align="justify">
												<ul>
												  <li>Customers would be able to Login</li>
												  <li>Customers would be able to view their account after successful Login.</li>
												  <li>Customers would be able to add/edit/delete all their details.</li>
												  <li>Customers would be able to request for their user name in     case they forgets their user name Login (Details will be mailed to the     Customer)</li>
												  <li>Customers would be able to request for their password in     case they forgets their password (Login Details will be mailed to the     Customer)</li>
												  <li>Customers would be able to change the password (Login Details will be mailed to the Customer)</li>
												</ul>
											  </div>
											  <p align="justify"><strong>Function of Customers</strong></p>
											  <div align="justify">
												<ul>
												  <li>Customers will be able to login/register into the website.</li>
												  <li>Customers will be able to create an account after submitting their email id, nam e, address, etc on the website.</li>
												  <li>Customers will also be able to easily search for products   by   using different keywords like name, category wise etc and will be     able to refine their results by using filters such as price, product     type etc. on the website.</li>
												  <li>Customers will be able to view the products with details, images, zoom in option etc. on the website.</li>
												  <li>Customers will have the ability to customize their products by submitting the information on the website.</li>
												  <li>Customers can view the events posted by the admin on the website.</li>
												  <li>Customers can submit their reviews on the products listed on the website.</li>
												  <li>Customers will be able to place orders on the website.</li>
												  <li>Customers will be able to check their order status on the website.</li>
												  <li>Customers will be able to use the facility of shopping cart on the website.</li>
												  <li>Customers will be able to make payments for their orders by using integrated payment gateway given by the Client.</li>
												  <li>Customers will be able to choose their mode of shipping using integrated shipping gateway on the website.</li>
												  <li>Customers will be able to view the shipping details on the website.</li>
												  <li>Customers will be able to provide delivery address for each order made on the website.</li>
												  <li>Customers will be able to receive an email for confirmation after an order placed on the website.</li>
												  <li>Customers will be able to receive newsletter after     submitting their name and email id on the website and this section will     be managed by admin.</li>
												  <li>Customers will be able to share the link of the website on the social networking website like facebook, twitter etc.</li>
												  <li>Customers will be able to view FAQ on the website.</li>
												</ul>
											  </div>
											  <p align="justify"><strong>My Account Control Panel for Customers</strong><br>
												Customers will be able to operate a host of functions from their<br>
												Account control panel. The control panel will carry the following modules:<br>
												<strong>My Details</strong></p>
											  <div align="justify">
												<ul>
												  <li>Customers can fill in details about them</li>
												  <li>Customers can change any information anytime</li>
												  <li>My Orders</li>
												  <li>Customers can check status of their instant orders</li>
												  <li>Customers can check past purchases</li>
												  <li>Customers can print the orders</li>
												  <li>Customers can review active orders</li>
												  <li>Payments</li>
												  <li>Customers can view all their old payment transactions</li>
												  <li>Customers can make payment for new orders</li>
												</ul>
											  </div>
											  <p align="justify"><strong>Search</strong><br>
												Customers will also be able to easily search for products by   using   different keywords like name, category etc and will be able to   refine   their results by using filters such as price etc. on the website.     They will be able to view list of content that match to their searched     criteria.<br>
												<strong>Advanced Search</strong><br>
												In this section customers will be able to customize the search     based on their choice. Customers will be able to select preference and     will be able to view the list that matches with their searched criteria<br>
												<strong>Products</strong><br>
												Products will also be sorted according to the categories and   sub   categories. Once a category is selected all the products will come   out   listed along with the image and other necessary details. If a     customer clicks on the product he will be taken to a page where the     complete details about the product is listed. They can view different     images of the particular product, read specifications about it and can     add that product to the shopping cart. Admin will be able to manage   this   section from the backend.<br>
												<strong>Products Catalog</strong><br>
												The site will have the following categories and sub categories listed which will be modifiable form the back end.</p>
											  <div align="justify">
												<ul>
												  <li>Product image</li>
												  <li>Price</li>
												  <li>Specifications</li>
												  <li>Ratings</li>
												  <li>Add to cart</li>
												</ul>
											  </div>
											  <p align="justify"><strong>Products Information</strong><br>
												Products page will carry information about the product being displayed. The page will carry:</p>
											  <div align="justify">
												<ul>
												  <li>Product information</li>
												  <li>Product images</li>
												  <li>Product Price</li>
												  <li>View Full Size</li>
												  <li>Zoom</li>
												  <li>Price</li>
												  <li>Add to cart</li>
												</ul>
											  </div>
											  <p align="justify"><strong>Customize</strong><br>
												In this section customers will be able to make their   customization   on the listed products by submitting their information on   the   website.<br>
												<strong>Events</strong><br>
												In this section customers will be able to view the various   events   details posted by the admin on the website. Admin can manage this     section from backend.<br>
												<strong>Review</strong><br>
												In this section customers can submit their reviews on the website. Admin can manage this section from backend.<br>
												<strong>Shopping Cart</strong><br>
												The shopping cart will allow the customers to manage their     shopping in an easy and convenient way. The shopping cart will carry the     following features:</p>
											  <div align="justify">
												<ul>
												  <li>Customers can view their order history and order statuses</li>
												  <li>All orders will be stored in the database for fast and efficient retrieval</li>
												  <li>Temporary shopping cart for guests and permanent shopping cart for customers</li>
												  <li>Foreseen checkout procedure</li>
												  <li>Add/Update carts in real time</li>
												  <li>Shipping options</li>
												  <li>Full product stock control</li>
												  <li>Bulk product addition/modifying</li>
												  <li>Bulk category addition/modifying</li>
												  <li>Enabling/disabling products and categories with one click</li>
												  <li>Mini-cart presence on all pages</li>
												  <li>Payment options</li>
												  <li>Shipping and billing address</li>
												  <li>Recalculate the total value</li>
												</ul>
											  </div>
											  <p align="justify"><strong>Check out</strong><br>
												Check out will allow the customers to carry the following features:</p>
											  <div align="justify">
												<ul>
												  <li>Login or register</li>
												  <li>Choose delivery options</li>
												  <li>Order summary</li>
												  <li>Enter billing and payment details</li>
												  <li>Select Shipping options</li>
												  <li>Complete order</li>
												</ul>
											  </div>
											  <p align="justify"><strong>Shipping</strong><br>
												Customers will be able to view shipping prices for their   purchased   products on the website. Shipping will be according to zip   code   address of the customers. After payment paid by the customers,   admin   will be able to ship the products to the customers. Website will   be   integrated to the shipping gateway provided by the Client. Admin will     be able to manage this section from the backend.</p>
											  <p align="justify"><strong>Payment gateway</strong><br>
												Customers will be able to make payments for the purchased   products   on the website by using integrated payment gateway given by the     Client. Admin will be able to manage this section from the backend.<br>
												<strong>Social Media Integration</strong><br>
												The website will be integrated with various social networking     websites like facebook, Twitter etc. Customers can only share the link     of the website on these social networking sites.<br>
												<strong>Newsletter</strong><br>
												Customers will be able to subscribe for the newsletter on the     website by submitting e-mail id and name on the website. This section     will be managed by admin from the backend.<br>
												<strong>Contact us</strong><br>
												The contact us page will list contact details of the Client   along   with a contact us from and FAQ search option. Features include:</p>
											  <div align="justify">
												<ul>
												  <li>Contact us form</li>
												  <li>Contact Us Form Fields</li>
												  <li>First Name</li>
												  <li>Last name</li>
												  <li>Email</li>
												  <li>Contact (Drop down menu options)</li>
												  <li>Message</li>
												</ul>
											  </div>
											  <p align="justify">Contact options: General Enquiry and other enquiry.   Others   will follow Contact form will be shown to the customers in all   the pages   of the website.<br>
												<strong>FAQ</strong><br>
												This section will list out the FAQ’s listed on the site. The FAQ’s will be maintained by the<br>
												administrator. In this section all the questions will appear category vise for the ease of customers.<br>
												<strong>Email Notifications</strong></p>
											  <div align="justify">
												<ul>
												  <li>Customers will receive a notification after registration.</li>
												  <li>Customers will receive notification after newsletter subscription.</li>
												  <li>Customers will receive a notification after confirmation of order.</li>
												  <li>Customers will receive a notification after payment</li>
												</ul>
											  </div>
											  <p align="justify"><strong>5. Administrative Panel</strong><br>
												The back end of the website will be power packed with an     administrative panel to manage the updation of data at the front as well     as back end. Following are the key functionalities</p>
											  <div align="justify">
												<ul>
												  <li>Customers Management</li>
												  <li>Product Management</li>
												  <li>General Management</li>
												  <li>Order Management</li>
												  <li>Content Management</li>
												  <li>Reports Management</li>
												</ul>
											  </div>
											  <p align="justify"><strong>Customers Management</strong></p>
											  <div align="justify">
												<ul>
												  <li>Admin will be able to Manage the customers of the site</li>
												  <li>Admin will be able to Add / Delete customers of the site</li>
												  <li>Admin will be able to approve / reject the registration of the customers</li>
												  <li>Admin will be able to View the list of all customers of the site</li>
												  <li>Admin will be able to Search the list of all customers of the site</li>
												  <li>Admin will be able to Activate or Deactivate the customers of the site</li>
												</ul>
											  </div>
											  <p align="justify"><strong>Products Management</strong></p>
											  <div align="justify">
												<ul>
												  <li>Admin will be able to manage the Categories and Sub Categories of products on the website.</li>
												  <li>Admin will be able to add / Edit / Delete the Categories and the Sub Categories of products with codes of the website.</li>
												  <li>Admin will be able to View the list of all Categories and the Sub Categories of products of the website.</li>
												  <li>Admin will be able to Activate / Deactivate the Categories and the Sub Categories of products of the website.</li>
												  <li>Admin will be able to add / Edit / Delete products on the website.</li>
												</ul>
											  </div>
											  <p align="justify"><strong>General Management</strong></p>
											  <div align="justify">
												<ul>
												  <li>Manage Shipping</li>
												  <li>Manage reviews</li>
												  <li>Manage Events</li>
												  <li>Manage Inventory</li>
												</ul>
											  </div>
											  <p align="justify"><strong>Order Management</strong></p>
											  <div align="justify">
												<ul>
												  <li>Admin can Manage the orders received by the Site</li>
												  <li>Admin can Add / Delete the orders received by the Site</li>
												  <li>Admin can View the List of all orders received by the Site</li>
												  <li>Admin can Search the orders received by the Site</li>
												  <li>The customer will get notified via email once the order is     confirmed with all t he relevant details of the products purchased in     that order.</li>
												</ul>
											  </div>
											  <p align="justify"><strong>Content Management</strong><br>
												Admin will be able to add/delete text/images/videos of the   items   on the site. The admin will be provided a rich interface editor   which   will enable him to create as many pag es as required. Admin will   be   able to add text, images, links etc to the pages and those pages can     be linked to any other pages on the same site.<br>
												<strong>Reports Management</strong><br>
												Admin will be able to generate reports in a printable format for the following:</p>
											  <div align="justify">
												<ul>
												  <li>List of Customers</li>
												  <li>Payment Reports</li>
												  <li>Sales Reports</li>
												  <li>Inventory Reports</li>
												</ul>
											  </div>
											  <p align="justify">The admin will be able to apply filters date wise, name wise etc<br>
												The admin will be able to export the reports in other formats like excel, csv etc</p>
											  <p align="justify"><strong>6. Standardizations</strong><br>
												<strong>Design Standards</strong><br>
												Contrary to the general perception designing web applications   is   much more than just putting in good looking graphics and flash     components. It requires much expertise to able it to run smoothly. We     ensure that by:</p>
											  <div align="justify">
												<ul>
												  <li>Designing the web application to make sure that the web pages will appear without horizontal scroll bar in all resolutions.</li>
												  <li>The web site can be viewed on all the four popular browsers i.e.</li>
												  <li>Internet Explorer</li>
												  <li>Firefox Mozilla</li>
												  <li>Safari</li>
												  <li>Chrome</li>
												  <li>Uniform Navigations in all the web pages of the application.</li>
												</ul>
											  </div>
											  <p align="justify"><strong>Testing Standards</strong><br>
												We make sure each of our services undergoes rigorous testing so     that it becomes 100% free of bugs. All the standard methods of white     and black box testing are in place to achieve this feat. We:</p>
											  <div align="justify">
												<ul>
												  <li>Prepare the test cases based on design and functionality.</li>
												  <li>Ensure a cosmetic bug test before delivery.</li>
												  <li>Test the embedded links exhaustively to make sure no errors are there.</li>
												  <li>Validate all web applications</li>
												  <li>Implementing Cascading Style Sheets (CSS) for each of the application.</li>
												  <li>All code will be compliant to W3C</li>
												</ul>
											  </div>
											  <p align="justify"><strong>Delivery Standards</strong><br>
												MIZAN IT would promise to deliver the final project in the following ways</p>
											  <div align="justify">
												<ul>
												  <li>Directly hosting on to servers mentioned by Client and deploys the application and database.</li>
												  <li>A document briefing all details of the files and Database structure will be released after the final payment.</li>
												  <li>Deliver the complete source code, with the database   scripts   in form of “PATCH” (which is the standard way followed by MIZAN IT, for   delivering source code).</li>
												  <li>All copyrights to the website will be held by Client.</li>
												</ul>
											  </div>
											  <p align="justify"><strong>7. Delivery Schedule</strong></p>
											  <div align="justify">
												<table class="table table-striped" width="642" cellspacing="0" cellpadding="0">
												  <tbody>
													<tr>
													  <td width="61"><br>
														Weeks</td>
													  <td width="581"><p align="center">Key Deliverables (Modules)</p></td>
													</tr>
													<tr>
													  <td width="61"><p align="center">1</p></td>
													  <td width="581"><p>Knowledge transfer, Working on the layout,   Working on the   functional requirements, Locking in the database   design, Workings on   Specific requirements.</p></td>
													</tr>
													<tr>
													  <td width="61"><p align="center">2</p></td>
													  <td width="581"><p>Development of module for customer’s registration, my account panel, search, advanced search with corresponding admin panel.</p></td>
													</tr>
													<tr>
													  <td width="61"><p align="center">3</p></td>
													  <td width="581"><p>Development of module for products   information, products   catalogue, Customize, Events, shopping cart, and   check out with   corresponding admin panel.</p></td>
													</tr>
													<tr>
													  <td width="61"><p align="center">4</p></td>
													  <td width="581"><p>Development of module for payment gateway integration, shipping gateway integration, reviews and with corresponding admin panel.</p></td>
													</tr>
													<tr>
													  <td width="61"><p align="center">5</p></td>
													  <td width="581"><p>Development of module for social media integration, newsletter, static pages and contact us page with corresponding admin panel.</p></td>
													</tr>
													<tr>
													  <td width="61"><p align="center">6</p></td>
													  <td width="581"><p>Exhaustive testing, Ensuring Browser compatibility, Final delivery.</p></td>
													</tr>
												  </tbody>
												</table>
											  </div>
											  <p align="justify"><strong>8. Process Flow</strong></p>
											  <div align="justify">
												<ul>
												  <li>Analysis</li>
												  <li>Design</li>
												  <li>Review</li>
												  <li>Implement</li>
												  <li>Testing</li>
												  <li>Project</li>
												  <li>Delivery</li>
												  <li>Our Methodology… Best quality with the best design</li>
												  <li>Support</li>
												</ul>
											  </div>
											  <p align="justify"><strong>9. Requirements from Client</strong><br>
												MIZAN IT would require the following from Client. This information would be solely used for the project purpose.</p>
											  <div align="justify">
												<ul>
												  <li>Detailed document in case any more features need to be added on the website.</li>
												  <li>Point of contact to discuss the updates on a daily or a weekly basis as preferred by Client.</li>
												  <li>Details like address, telephone numbers, website, photos etc pertaining to all the categories mentioned in the methodology.</li>
												</ul>
											  </div>
											  <p align="justify"><strong>10. MIZAN IT’ Commitment</strong><br>
												MIZAN IT’ deliverables catering to website design and   development   and custom web applications development space are as   follows:</p>
											  <div align="justify">
												<ul>
												  <li>Fully functional site with the aforementioned deliverables.</li>
												  <li>ETA of the project would be 6 weeks. Further changes in the deliverables may extend or reduce the ETA.</li>
												  <li>MIZAN IT works on a strict schedule and promises to   deliver   as per the agreed time frame. However, we take a standard   deviation of   10% due to force majeure.</li>
												</ul>
											  </div>
											</div>
											<p align="justify">&nbsp;</p>
										  </div>
										  <h4>&nbsp;</h4>
										</div>
								  <div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									
								  </div>
								</div>
							  </div>
							</div>
						<h4 class="text-center" style="color:#F02D28;text-width:bold;">E_Commerce Site & Deveopment </h4>
				</figure>
			
				<figure class="mix work-item SEO">
					<a class="service1" href="#SEO" data-toggle="modal" data-target="#SEO"rel="works" title="Write Your Image Caption Here"><h3>View Details</h3><img src="img/works/item-8.jpg" alt=""></a>
		 
							<!-- Modal -->
							<div class="modal fade" id="SEO" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							  <div class="modal-dialog" role="document">
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h2 style="color:#1A7FEC; font-width:bold;" class="modal-title" id="myModalLabel">Search Engine Optimization (SEO)</h2>
								  </div>
								  <div id="tooplate_content">
									  <div id="Layer27">
										<div id="Layer7">
										  <div align="justify">
											<div id="Layer">
											  <p align="justify">Without the services of the search engines our   websites may be seen     by just a handful of people. Every popular   search engine has its own     terms on how they rank a particular   website or a page within the site.     Webmasters and web service   providers keep constant track of the ever     changing algorithms so   that they can help the websites they are     servicing to perform at   optimal levels. Though Google is the most     popular search engine   around the globe, we cannot afford to ignore the     others because, as   website owners, we have no knowledge about specific       customer/demographic preferences. </p>
											  <p align="justify">Search Engine Optimization is an integral   part of   our web promotion   services. A host of methods and tools are     employed in ensuring that   almost every popular search engine across     the digital world finds your   website and lists it on the first page.      This is an essential measure to   ensure that your website receives     continuous targeted traffic. Search   Engine Marketing employs many of     the tools employed in search engine   optimization though the expertise     needed for this job is different from   search engine optimization     itself. </p>
											  <p align="justify">After your website design and   functionalities are   discussed, website   promotion will generally start   as soon as the   site is hosted and   submitted to the search engines.   Traffic building   strategies like link   building, directory submission,   bookmarking,   blog posting and on-site   SEO measures will also be   launched   concurrently to ensure that traffic   to your websites starts   at a   quick pace and grows consistently. Even   websites that are long     established and drawing millions of visitors   everyday need to be     consistently nurtured to help them retain their   coveted position.     Identifying appropriate keywords and using those   keywords diligently     can be done only by experienced professionals.</p>
											  <p align="justify">We understand that every business is unique   and   so are SEO needs. Yet   the bottom-line is the relation between     investment and sales. We are   driven by developing the unique and     customized SEO strategies to provide   sustainable results. Our ethical     and organic SEO approach helps your   business achieve success and the     best ROI. </p>
											  <p align="justify">These are the key points includes to optimizing your website:</p>
											  <p align="justify">1.Search Engine Optimization content development for the site with targeted                                   keywords.<br>
												2.Develop keyword density in links.<br>
												3.Images optimization (load time with alt tag)<br>
												4.Optimization all the pages of the site with keywords<br>
												5.Creation of Search Engine Friendly Site Map<br>
												6.Spell Checking for all the WebPages<br>
												7.Load Time Monitoring &amp; Reporting <br>
												8.Link Popularity development<br>
												9.HTML Validation Checking <br>
												10.HTML Design Analysis &amp; Changes accordingly <br>
												11.Browser Compatibility checking <br>
												12.Keyword Density maintenance <br>
												13.Backlinks Development<br>
												14.Creation of Optimized Robots.txt File<br>
												15. Develop inbound links for the   pages (it will increase pr of the site)<br>
												16.Develop outbound links for all the pages (it will increase pr of the site)<br>
												17.Work on meta tag with googlebot.<br>
												18.Bloging for develop link popularity<br>
												19.Maintain forum for the pages to reach more visitor.<br>
												20.Depth Keyword Research &amp;  Suggestions Related to Your Industry <br>
												21.Targeting of Unlimited Keyword Phrases<br>
												22.Link Reputation Analysis <br>
												23.Linking Strategy and Management.<br>
												24.Link Management.<br>
												25.PPC Management.<br>
												26.Pay for Placement.<br>
												27.Organic Search Engine Optimization.<br>
												28.Blogs Development.<br>
												29.Directory Management.<br>
												30.Internet Marketing.</p>
											  <p align="justify">Cost: 15000 taka/month</p>
											  <p align="justify">Estimate time: Minimum 6 month for 1 to 3 pages </p>
											</div>
										  </div>
										</div>
									  </div>
									</div>
								  <div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									
								  </div>
								</div>
							  </div>
							</div>
						<h4 class="text-center" style="color:#F02D28;text-width:bold;">Search Engine Optimization</h4>
				</figure>
				
             
				<!-- <figure class="mix work-item mail_Marketinge">
					<a class="service1" href="#mail_Marketinge" data-toggle="modal" data-target="#mail_Marketinge"rel="works" title="Write Your Image Caption Here"><h3>View Details</h3><img src="img/works/item-6.jpg" alt=""></a>
		 
						
							<div class="modal fade" id="mail_Marketinge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							  <div class="modal-dialog" role="document">
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h2 style="color:#1A7FEC; font-width:bold;"class="modal-title" id="myModalLabel">E-mail Marketing</h2>
								  </div>
								  <div id="tooplate_content">
									  <div id="Layer27">
										<div id="Layer7">
										  <div align="justify">
											<div id="Layer">
											  <div id="Layer16">
												<p align="justify"><strong>Why Email Marketing?</strong></p>
												<div align="justify">
												  <ul type="disc">
													<li>Directly reach to the targeted people.</li>
													<li>Lowest cost and highest benefit.</li>
													<li>Advertising space is not a issue. You can use unlimited       space for advertisement.</li>
													<li>Website address and email address can be linked       directly.</li>
													<li>Email will delivered by using your company email. So       you will get any kind of reply in your email from clients.</li>
													<li>You can increase your sells by email marketing. Beside       this your company gets a brand position.</li>
													<li>Quick&nbsp;response &amp; quick delivery.</li>
													<li>You can use both picture and text. Beside this, you can       use feedback form in emai</li>
												  </ul>
												</div>
												<p align="justify"><strong>Why you choose&nbsp;email marketing services from MIZAN IT?</strong></p>
												<div align="justify">
												  <ul type="disc">
													<li>Automatically email delivery by Email marketing       software.</li>
													<li>No duplicate email. Our software removes email if it       get any duplicate in the email list.</li>
													<li>All verified email. We use our customized software to       verify all email.</li>
													<li>80-90% inbox delivery.</li>
													<li>Auto generated report for clients (Inbox report, Spam       report, Bounce report).</li>
													<li>MIZAN IT is pioneer of Email marketing in Bangladesh.       So we always maintain our reputation</li>
													<li>Free consultancy for better advertisement.</li>
													<li>Cost effective.</li>
													<li>15000+ satisfied clients worldwide.</li>
													<li>Specific Clients area.</li>
													<li>24*7&nbsp;customer support.</li>
													<li>1 by 1 email sending system.</li>
													<li>SMTP server with dedicated IP</li>
													<li>Spam and bounce detector</li>
													<li>Picture can add in body. No need to attachment</li>
												  </ul>
												</div>
												<p align="justify"><strong>Our Database:</strong><br>
												  <strong>Corporate Area</strong><strong>: </strong>Corporate     employer, CTO, CEO,  Managing director, Club members(Like Dhaka club,     Gulshan club, Uttara Club,  Lions Club etc), Different organization     member(Like BGMEA, BASIS, FBCCI etc),  Different company owner and     directors etc. &ndash;<strong>Total 4,50,000+ email</strong></p>
												<p align="justify"><strong>Regular Area</strong><strong>:</strong> Student and Corporate employee.-<strong>Total 7,50,000+ email</strong></p>
												<p align="justify"><strong><u><br>
												  Email marketing Package Cost:</u></strong></p>
											  </div>
											  <div id="Layer17">
												<table class="table table-striped" width="600" cellspacing="0" cellpadding="0" border="1">
												  <tbody>
													<tr>
													  <td width="133" valign="top"><b>Package </b></td>
													  <td width="165" valign="top"><b>Details </b></td>
													  <td width="91" valign="top"><p align="center">Corporate    (4,50,000+) </p></td>
													  <td width="89" valign="top"><p align="center">Student    (7,50,000+)</p></td>
													  <td width="110" valign="top"><p align="center">Total <br>
														(12,00,000+) </p></td>
													</tr>
													<tr>
													  <td width="133" valign="top"><p>Basic package </p></td>
													  <td width="165" valign="top"><p>1 times marketing </p></td>
													  <td width="91" valign="top"><p align="center">12,000    Taka</p></td>
													  <td width="89" valign="top"><p align="center">18,000    Taka</p></td>
													  <td width="110" valign="top"><p align="center">28,000    Taka</p></td>
													</tr>
													<tr>
													  <td width="133" valign="top"><p>Basic plus package </p></td>
													  <td width="165" valign="top"><p>2 times marketing in a month </p></td>
													  <td width="91" valign="top"><p align="center">23,000    Taka</p></td>
													  <td width="89" valign="top"><p align="center">35,000    Taka</p></td>
													  <td width="110" valign="top"><p align="center">55,000    Taka</p></td>
													</tr>
													<tr>
													  <td width="133" valign="top"><p>Premium package </p></td>
													  <td width="165" valign="top"><p>6 times marketing in 3 month </p></td>
													  <td width="91" valign="top"><p align="center">65,000    Taka</p></td>
													  <td width="89" valign="top"><p align="center">95,000    Taka</p></td>
													  <td width="110" valign="top"><p align="center">1,50,000    Taka</p></td>
													</tr>
													<tr>
													  <td width="133" valign="top"><p>Premium plus package </p></td>
													  <td width="165" valign="top"><p>12 times marketing in 6 month </p></td>
													  <td width="91" valign="top"><p align="center">1,20,000    Taka</p></td>
													  <td width="89" valign="top"><p align="center">1,80,000    Taka</p></td>
													  <td width="110" valign="top"><p align="center">2,90,000    Taka</p></td>
													</tr>
													<tr>
													  <td width="133" valign="top"><p>Deluxe Package </p></td>
													  <td width="165" valign="top"><p>24 times marketing in 12 month </p></td>
													  <td width="91" valign="top"><p align="center">2,20,000    Taka</p></td>
													  <td width="89" valign="top"><p align="center">3,50,000    Taka</p></td>
													  <td width="110" valign="top"><p align="center">4,50,000    Taka</p></td>
													</tr>
												  </tbody>
												</table>
											  </div>
											  <div id="Layer20"><strong>Email marketing</strong><br>
												<strong>“There is no best way to promote your product without  advertisement.”</strong></div>
											  <div id="Layer21">
												<p align="justify"><strong><u>Email  marketing requirement:</u></strong></p>
												<div align="justify">
												  <ul>
													<li>Email  body (Content may be picture or plain text or picture, text both or html  formate).</li>
													<li>Email  subject (Not more than 80 characters).</li>
													<li>Company  name (Not more than 40 characters)..</li>
													<li>Email  address.</li>
												  </ul>
												</div>
												<p align="justify"><strong><u>What you will get in  report?</u></strong></p>
												<div align="justify">
												  <ul>
													<li>How much email delivered in INBOX</li>
													<li>How much email delivered in SPAM</li>
													<li>How much email undelivered</li>
													<li>Report will generate within 24 hour after complete  the full email marketing</li>
												  </ul>
												</div>
												<p align="justify"><strong><u>Some important information:</u></strong></p>
												<div align="justify">
												  <ul type="disc">
													<li>You       can ensure your email marketing from the report or visiting our office or       from response.</li>
													<li>We       do not provide email database report</li>
													<li>We       do not sale email address. We only send email by using our software and       server.</li>
													<li>We       can operate the email marketing only from our office. </li>
												  </ul>
												</div>
												<p align="justify">&nbsp;</p>
											  </div>
											  <div id="Layer24">
												<p align="justify"><strong><u>Payment Procedure:</u></strong></p>
												<div align="justify">
												  <ul type="disc">
													<li>100% advance payment for Trial package, Basic package,       Basic plus package, Premium package </li>
													<li>50% advance payment for Premium plus package and rest       50% after 3 month. </li>
													<li>25% advance payment for deluxe package and rest amount       will pay after 4 month. </li>
													<li>Payment will goes to “MIZAN IT” account pay check or       cash. </li>
													<li>Client must provide all content with work order. </li>
													<li>Design charge 2000 taka ( if needed )</li>
												  </ul>
												</div>
											  </div>
											  <p align="justify">&nbsp;</p>
											</div>
										  </div>
										</div>
									  </div>
									</div>
								  <div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									
								  </div>
								</div>
							  </div>
							</div>
						<h4 class="text-center" style="color:#F02D28;text-width:bold;">Mail Marketinge</h4>
				</figure> 
				-->
				
                
                   <figure class="mix work-item SMS_Marketing">
					<a class="service1" href="#SMS_Marketing" data-toggle="modal" data-target="#SMS_Marketing"rel="works" title="Write Your Image Caption Here"><h3>View Details</h3><img src="img/works/item-9.jpg" alt=""></a>
		 
							<!-- Modal -->
							<div class="modal fade" id="SMS_Marketing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							  <div class="modal-dialog" role="document">
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h2 style="color:#1A7FEC; font-width:bold;" class="modal-title" id="myModalLabel">SMS Marketinge</h2>
								  </div>
								  <div id="tooplate_content">
									  <div id="Layer27">
										<div id="Layer7">
										  <div align="justify">
											<div id="Layer">
											  <div id="Layer16">
												<div id="Layer29">
												  <p align="justify"><strong>SMS Gateway:</strong></p>
												  <p align="justify">FEATURES of Brand SMS:</p>
												  <p align="justify"><i class="fa fa-comment"></i> Send SMS from your Brand Name. (Sender ID Maximum 11 Characters)<br>
													<i class="fa fa-comment"></i> SMS broadcasting speed: 200 &ndash; 300 sms per sec.<br>
													<i class="fa fa-comment"></i> Easy web interface utility.<br>
													<i class="fa fa-comment"></i> Password Protected Web Access.<br>
													<i class="fa fa-comment"></i> Free Demo Account to Test and Check our Serivces.<br>
													<i class="fa fa-comment"></i> All Types of SMS Supported (Normal Text SMS, Unicode SMS and Flash SMS).<br>
													<i class="fa fa-comment"></i> Send messages to upto 1080 characters (160 characters/SMS).<br>
													<i class="fa fa-comment"></i> Worldwide Access.<br>
													<i class="fa fa-comment"></i> SMPP connectivity.<br>
													<i class="fa fa-comment"></i> API (Application Programing Interface) Free of Cost.<br>
													<i class="fa fa-comment"></i> Upload the number list from an excel sheet. CSV Format or Notpad file (Txt.) Format.<br>
													<i class="fa fa-comment"></i> Unlimited Address Book and Group Contacts.<br>
													<i class="fa fa-comment"></i> Free Phone and Email support.<br>
													<i class="fa fa-comment"></i> No hidden costs.<br>
													<i class="fa fa-comment"></i> Send single or group SMS on one Click &amp; Auto Schedule.<br>
													<i class="fa fa-comment"></i> Real Time SMS Delivery Report.<br>
													<i class="fa fa-comment"></i> UPto 48 Hours resend queue if the destination number is out of network or switched off.<br>
													<i class="fa fa-comment"></i> History log (Instant, Mass SMS, and Scheduled etc.).<br>
													<i class="fa fa-comment"></i> Send SMS on scheduled date and time.<br>
													<i class="fa fa-comment"></i> Management Information Report: Complete MIS with downloadable report in MS-Excel format.<br>
													<i class="fa fa-comment"></i> Pricing: Fare Pricing for all Networks and Destination<br>
													<i class="fa fa-comment"></i> Payment Type: Prepaid<br>
													<i class="fa fa-comment"></i> Payment Option: Payment through Bkash or Bank Transfer<br>
													<i class="fa fa-comment"></i> Secutity: 100% Security and Privacy Protection</p>
												  <p align="justify">Gateway Price: 5,000 taka </p>
												  <p align="justify">SMS cost: 0.60 taka per SMS (Only for Bangladesh) </p>
												</div>
												<div id="Layer30">
												  <p align="justify"><strong>SMS Marketing:</strong></p>
												  <p align="justify"> Mobile    phone is the most popular technology of   our daily life. This   technology has  become the main communication   media. SMS is such a   messaging service which may  create instant   response through mobile   phone. We like to inform you that our  Mizan   IT team has some qualified   IT specialists who are skilled in SMS    technology. So we are so much   confident to deliver any kind SMS   solution like  bulk sms, push-pull   sms, web based sms service.<br>
												  </p>
												  <p align="justify"><strong>Scope of Our SMS Services</strong></p>
												  <div align="justify">
													<ul type="disc">
													  <li>Job Notification</li>
													  <li>General Info (Office Address, Service Details)</li>
													  <li>Specific Information (Training/Cultural Program)</li>
													  <li>Notice / Greeting (Official / Occasional for target       people)</li>
													  <li>Reminder for Due Payment / Due Inquiry</li>
													  <li>Events: Voting, Quiz contest (From on different       programs of various media like TV/Radio/Newspaper)</li>
													  <li>Event Notification (Meeting ,Program ,others, Training)</li>
													</ul>
												  </div>
												  <p align="justify"><strong>Benefits of Cell/Mobile Phone SMS Services</strong></p>
												  <div align="justify">
													<ul type="disc">
													  <li>Now sms is a very effective way of direct       communication.</li>
													  <li>People always check their SMS quickly</li>
													  <li>It has the power to create instant response</li>
													  <li>The response is very helpful to boost up your business</li>
													  <li>Cheap cost marketing communication</li>
													</ul>
												  </div>
												  <p align="justify"><strong>Database:</strong><br>
													<strong>Corporate Area: </strong>Total 5,50,000+  Mobile No</p>
												  <div align="justify">
													<ul type="disc">
													  <li>Club       member: 27,146</li>
													  <li>Organization       member: 42,863</li>
													  <li>Association:       45286</li>
													  <li>Company       owner: 45,023</li>
													  <li>Top       management (CEO, CTO, COO, DIRECTOR, MANAGER): 61,761</li>
													  <li>Mobile       bill more than 2000 taka per month: 2,35,917</li>
													  <li>Government       top employee: 23,112</li>
													  <li>Doctors:       25,443</li>
													  <li>Advocate       and Barrister: 22,291</li>
													  <li>Engineer:       22,993</li>
													  <li>Dhaka       reporters unity: 2518</li>
													  <li>Chartered       accountant: 6734</li>
													  <li>Bar       associates: 2,653</li>
													</ul>
												  </div>
												</div>
												<div id="Layer31">
												  <p align="justify"><strong>Regular Area:</strong> Student and Corporate employee and area wise.-<strong>Total 18,  00,000+ Mobile No</strong></p>
												  <p align="justify"><strong>SMS marketing package cost:</strong></p>
												  <div align="justify">
													<table class="table table-striped" width="577" cellspacing="0" cellpadding="0">
													  <tbody>
														<tr>
														  <td width="258" valign="top"> Package</td>
														  <td width="319" valign="top" align="left"><p align="center"><strong>Cost/SMS</strong></p></td>
														</tr>
														<tr>
														  <td width="258" valign="top"><p align="center">1,000 - 4,999</p></td>
														  <td width="319" valign="top"><p align="center">1.20    taka </p></td>
														</tr>
														<tr>
														  <td width="258" valign="top"><p align="center">5,000 - 9,999</p></td>
														  <td width="319" valign="top"><p align="center">1.00     taka</p></td>
														</tr>
														<tr>
														  <td width="258" valign="top"><p align="center">10,000 - 19,999</p></td>
														  <td width="319" valign="top"><p align="center">0.90    taka</p></td>
														</tr>
														<tr>
														  <td width="258" valign="top"><p align="center">20,000 - 49,999</p></td>
														  <td width="319" valign="top"><p align="center">0.80 taka</p></td>
														</tr>
														<tr>
														  <td width="258" valign="top"><p align="center">50,000 - 99,999</p></td>
														  <td width="319" valign="top"><p align="center">0.75 taka</p></td>
														</tr>
														<tr>
														  <td width="258" valign="top"><p align="center">1,00,000+</p></td>
														  <td width="319" valign="top"><p align="center">0.70    taka</p></td>
														</tr>
													  </tbody>
													</table>
												  </div>
												</div>
												<p align="justify">&nbsp;</p></div><div id="Layer24"><div align="justify">
												</div>
											  </div>
											  <p align="justify">&nbsp;</p>
											</div>
										  </div>
										</div>
									  </div>
									</div>
								  <div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									
								  </div>
								</div>
							  </div>
							</div>
						<h4 class="text-center" style="color:#F02D28;text-width:bold;">SMS Marketinge</h4>
				</figure>
				
				<figure class="mix work-item Software_Development">
					<a class="service1" href="#Software_Development" data-toggle="modal" data-target="#Software_Development"rel="works" title="Write Your Image Caption Here"><h3>View Details</h3><img src="img/works/item-7.jpg" alt=""></a>
		 
							<!-- Modal -->
							<div class="modal fade" id="Software_Development" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							  <div class="modal-dialog" role="document">
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h2 style="color:#1A7FEC; font-width:bold;" class="modal-title" id="myModalLabel">Software Development</h2>
								  </div>
								  <div id="tooplate_content">
										  <div id="Layer27">
											<div id="Layer7">
											  <div align="justify">
												<div id="Layer">
												  <div id="Layer16">
													<div id="Layer29">
													  <p align="justify"><strong>Our Ready Products</strong>                        </p>
													</div>
													<div id="Layer31">
													  <div align="justify">
														<div id="Layer28">
														  <div>
															<div>
															  <div>
																<div>
																  <h3 style="color:#8DD241; padding-left:10px; font-size:20px" align="justify">Accounting Software</h3>
																  <p align="justify"><strong>Features:</strong></p>
																  <div align="justify">
																	<ol>
																	  <li>Chart Of Accounts</li>
																	  <li>Receipt Entries</li>
																	  <li>Payment Entries</li>
																	  <li>Contra Entries</li>
																	  <li>Journal Entries</li>
																	</ol>
																  </div>
																</div>
															  </div>
															</div>
														  </div>
														</div>
														<div id="Layer2">
														  <p align="justify"><strong>Facility:</strong></p>
														  <div align="justify">
															<ol>
															  <li>Unlimited User</li>
															  <li>Access from anywhere</li>
															  <li>Unlimited PC access</li>
															  <li>Individual access &amp; user panel</li>
															  <li>Low cost &amp; best support</li>
															</ol>
														  </div>
														</div>
														<p align="justify"><strong>Report</strong><br>
														  Balance Sheet, Profit And Loss Accounts, Cash Flow Statement, Trial Balance, Ledger Statement, Reconciliation</p>
														<div id="Layer32">
														  <h3 style="color:#8DD241; padding-left:10px; font-size:20px" align="justify">Online Accounting &amp; Inventory Software</h3>
														  <p align="justify"><strong>Features:</strong></p>
														  <div align="justify">
															<ol>
															  <li>Sales module</li>
															  <li>Purchase module</li>
															  <li>Item &amp; Inventory</li>
															  <li>Manufacturing</li>
															  <li>Banking &amp; Ledger</li>
															</ol>
														  </div>
														  <p align="justify"><strong>Facility:</strong></p>
														  <div align="justify">
															<ol>
															  <li>Unlimited User</li>
															  <li>Access from anywhere</li>
															  <li>Unlimited PC access</li>
															  <li>Individual access &amp; user panel</li>
															  <li>Low cost &amp; best support</li>
															</ol>
														  </div>
														</div>
														<div id="Layer34">
														  <p align="justify"><strong>Report</strong><br>
															Chart of Accounts, List of Journal Entries, GL Account     Transactions, Annual Expense Breakdown, Balance Sheet, Profit and Loss     Statement, Trial Balance, Tax Report, Audit Trail, Bank Statement, Bill     of Material Listing, Print Work Orders, Inventory Valuation Report,     Inventory Planning Report, Stock Check Sheets, Inventory Sales Report,     GRN Valuation Report, Inventory Purchasing Report, Inventory Movement     Report, Costed Inventory Movement Report, Item Sales Summary Report,     Supplier Balances, Aged Supplier Analyses, Payment Report, Outstanding     GRNs Report, Supplier Detail Listing, Print Purchase Orders, Print     Remittances, Customer Balances, Aged Customer Analysis, Customer Detail     Listing, Sales Summary Report, Price Listing, Order Status Listing,     Salesman Listing, Print Invoices, Print Credit Notes, Print Deliveries,     Print Statements, Print Sales Orders, Print Sales Quotations, Print     Receipts</p>
														  <h3 style="color:#8DD241; padding-left:10px; font-size:20px" align="justify">HR &amp; Payroll Software</h3>
														  <p align="justify"><strong>Features:</strong></p>
														  <div align="justify">
															<ol>
															  <li>Employee Information</li>
															  <li>Company Information</li>
															  <li>Attendance Bonus</li>
															  <li>Salary Grade</li>
															  <li>Shift Schedules</li>
															  <li>Leave, Fixed Bonus</li>
															  <li>Designation Bonus</li>
															  <li>Weekly Allowance</li>
															  <li>Entry System</li>
															  <li>Increment</li>
															  <li>Advance Loan</li>
															  <li>Salary</li>
															</ol>
														  </div>
														  <p align="justify"><strong>Report</strong><br>
															Daily present &amp; absent report, Leave report, Late report,   In   &amp; out report, Ot report, Attendance Register, OT Register, EOT     Register, Increment Report, Promotion Report, Job Card, Monthly Salary     Sheet, Pay Slip, Salary Summary Report, Maternity Benefit Report,     Festival Bonus summary, Advance salary sheet ** 100% Support in     Compliance &amp; Non Compliance.</p>
														</div>
													  </div>
												  </div>
													<p align="justify">&nbsp;</p></div><div id="Layer24"><div align="justify">
													</div>
												  </div>
												  <p align="justify">&nbsp;</p>
												</div>
											  </div>
											</div>
										  </div>
										</div>
								  <div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									
								  </div>
								</div>
							  </div>
							</div>
						<h4 class="text-center" style="color:#F02D28;text-width:bold;">Software_Development</h4>
				</figure>
				
				<figure class="mix work-item Logo_Design">
					<a class="service1" href="#Logo_Design" data-toggle="modal" data-target="#Logo_Design"rel="works" title="Write Your Image Caption Here"><h3>View Details</h3><img src="img/works/item-1.jpg" alt=""></a>
		 
							<!-- Modal -->
							<div class="modal fade" id="Logo_Design" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							  <div class="modal-dialog" role="document">
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h2 style="color:#1A7FEC; font-width:bold;" class="modal-title" id="myModalLabel">Logo Dsign</h2>
								  </div>
								  <div id="tooplate_content">
									<div id="Layer27">
									  <div id="Layer7">
										<div align="justify">
										  <div id="Layer">
											<div id="Layer16">
											  <div id="Layer29">
												<div id="Layer5">
												<p align="justify">Full colour logo design<br>
												  2 design concepts<br>
												  2   revisions<br>
												  7 digital formats provided on final design:<br>
												  ai, pdf,   gif, jpg, eps, doc, png<br>
												  10 business days turnaround<br>
												Logo   style guide</p>
												<p align="justify">Cost: 10,000 taka</p>
												<p align="justify">Creates corporate logo with great detail &amp; makes sure that you as the client are satisfied                     
												with the   entire experience &amp; result. We specialize in web logo design creations for your business. A well designed logo                       
												signals the identity of a company, its strength, services &amp; products offered. Logo design services offered by   Mizan-IT                     
												technologies&nbsp; will create a long lasting   impression on a customer's mind. We do not offer   readymade logo design but                    
												logo design tailored to   your company goals. We create original logo design   which stand out and are unique in their own right                       
												look &amp; feel for your organization or product.<br>
												  <br>Mizan-IT technologies will show you all the rough sketches along with business logo design. Mizan-IT technologies will not stop                     
												  developing your   new logo design until you are satisfied with it. Our   strategically developed highly appealing business logo                       
												  designs and company logos design will give your business the instant recognition. We specialize in designing all types   of                     
												  web logo designs for small businesses, corporate   logos, product logo designs, internet business   logos, fancy logo designs, classical logo designs,   
												  modern logo designs, print logos, stationary   designs, complete corporate identity, marketing logo   designs, festival logos, seasonal logo designs,                       
												  party logo designs, flashy logos, stylish logo designs, unique logo, creative logo, handmade logo and many more.<br><br>Our                     
												  professional logo designs developed for your   website can be easily printed. We provide you with   all these vector and                     
												  bitmap file formats for your   convenience (.eps, .ai, .cdr, .emf, .wmf, .psd,   .jpg, .gif and .bmp). We love to talk about                     
												  logo   design &amp; offer outstanding support. We want to make sure you get a logo &amp; are there to make sure it happens.                      
												  When you work with Mizan-IT technologies&nbsp; you are working with a strong reputable logo design firm that will be   here for                     
												  years to come. Once you submit information   about your company, our designers get to work   immediately to create your company
												  logo. You'll see   your first web logo design concepts within just 72 hours!</p>
												</div>
										  <p align="justify">&nbsp;</p></div></div>
										</div>
									  </div>
									</div>
								  </div>
								</div>
								  <div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									
								  </div>
								</div>
							  </div>
							</div>
						<h4 class="text-center" style="color:#F02D28;text-width:bold;">Logo_Design</h4>
				</figure>
				
			</div>
		

		</section>
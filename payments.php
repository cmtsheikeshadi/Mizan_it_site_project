<section id="Payments" class="Payments">
			<div class="container">
				<div class="row">
				
					<div class="sec-title text-center mb50 wow bounceInDown animated" data-wow-duration="500ms">
					
						<h2>Payments</h2>
						<div class="devider"><i class="fa fa-heart-o fa-lg"></i></div><br />
						<h3 class="text-center" >There are lot of way to pay the bill of Mizan IT Limited:</h3>
					</div>

					<!-- Payments item -->

				     <figure class="team-member col-md-4 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-duration="500ms">
					    <div class="mizanit">
							<img alt="250x250 ad" src="img/payments/logo.png">
							<p>Cash Payment System <br />Pay us in our office: <br />Mizan IT    Limited <br />558, Kazi Para (1st    Floor)
							<br />Mirpur, Dhaka    -1216. <br />Phone:    +88 02 9030095 <br />Website: www.mizan-it.com</p>
                        </div>   
					 
					     </figure>
						 <figure class="team-member col-md-4 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-duration="500ms">
					  
                         
                            <div class="img">
						      	<img src="img/payments/e.png" alt="Team Member" class="img-responsive">
							</div>
                            
                            
                                <p><strong>Account Name :</strong> MIZAN IT LIMITED <br />
                                    <strong>Account Number :</strong> 1181350091783 <br />
                                    <strong>Bank Name :</strong> EASTERN BANK LIMITED <br />
                                    <strong>Branch :</strong> Mirpur Dar-Us-Salam Road Branch </p>
                            
					 
					     </figure>
						 
						 <figure class="team-member col-md-4 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-duration="500ms">
					  
                         
                            <div class="img">
						      	<img src="img/payments/p.PNG" alt="Team Member" class="img-responsive">
							</div>
                            
                            
                                <p><strong>Receiver Name</strong>: Kazi Mizanur Rahman<br>
									<strong>Address</strong>: 558, Kazi Para (1st    Floor)<br>
										Mirpur, Dhaka -1216, Bangladesh.<br>
									<strong>Mobile</strong>: 01716-432392 </p> </p>
                            
					 
					     </figure>
						 
						 <figure class="team-member col-md-4 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-duration="500ms">
					  
                         
                            <div class="img">
						      	<img src="img/payments/m.png" alt="Team Member" class="img-responsive">
							</div>
                            
                            
                                <p><strong>Receiver Name</strong>: Kazi Mizanur Rahman<br>
								  <strong>Address</strong>: 558, Kazi Para (1st    Floor)<br>
								  Mirpur, Dhaka -1216, Bangladesh.<br>
								  <strong>Mobile</strong>: 01716-432392</p>
                            
					 
					     </figure>
                   	<!-- end Payments -->
			  
                    	<!-- Payments item -->
				     <figure class="team-member col-md-4 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-duration="500ms">
					  
                         
                            <div class="img">
						      	<img src="img/payments/w.png" alt="Team Member" class="img-responsive">
							</div>
                            
                            
                                <p><strong>Receiver Name</strong>: Kazi Mizanur Rahman<br>
								  <strong>Address</strong>: 558, Kazi Para (1st    Floor)<br>
								  Mirpur, Dhaka -1216, Bangladesh.<br>
								  <strong>Mobile</strong>: 01716-432392</p>
										
					 
					     </figure>
                   	<!-- end Payments -->
                    
                    <!-- Payments item -->
				     <figure class="team-member col-md-4 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-duration="500ms">
					  
                         
                            <div class="img">
						      	<img src="img/payments/b.png" alt="Team Member" class="img-responsive">
							</div>
                            
                            
                                <p><strong>bKash Account    Number : </strong>01716-432392<br />
									This is Personal bKash Account.
                                    </p>
                            
					 
					     </figure>
                   	<!-- end Payments -->
			  
              
              
						
				</div>
			</div>
		</section>